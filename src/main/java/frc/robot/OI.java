package frc.robot;

import frc.robot.utils.Controller;
import frc.robot.utils.PeriodicCaller;

/**
 * Class for interfacing with the driver's controllers
 */
public class OI {
	private static OI instance;
	private Controller driverController, operatorController;
	private PeriodicCaller controllerUpdater;
	
	private OI() {
		driverController = new Controller(0);
		operatorController = new Controller(1);
	}
	
	/**
	 * Constructs/Returns the OI instance
	 *
	 * @return OI instance
	 */
	public static OI getInstance() {
		return instance = instance == null ? new OI() : instance;
	}
	
	/**
	 * Get's the driver controller which should be at port 0
	 *
	 * @return driver's {@link Controller}
	 */
	public Controller getDriverController() {
		return driverController;
	}
	
	/**
	 * Get's the operator controller which should be at port 1
	 *
	 * @return operator's {@link Controller}
	 */
	public Controller getOperatorController() {
		return operatorController;
	}
	
	/**
	 * Updates controller's type
	 */
	public void updateControllers() {
		getDriverController().updateControllerType();
		getOperatorController().updateControllerType();
	}
	
	public void updateShuffleboard() {
		getDriverController().updateShuffleboard();
		getOperatorController().updateShuffleboard();
	}
	
	/**
	 * Starts updating the controllers' types
	 */
	public void startPeriodicControllerUpdate() {
		controllerUpdater = controllerUpdater == null ? new PeriodicCaller(1000, this::updateControllers) : controllerUpdater;
		try {
			if(!controllerUpdater.isAlive())
				controllerUpdater.start();
		} catch(IllegalThreadStateException ignored) {
		}
	}
	
	/**
	 * Stops updating the controllers' types
	 */
	public void stopPeriodicControllerUpdate() {
		if(controllerUpdater != null) {
			try {
				if(controllerUpdater.isAlive())
					controllerUpdater.interrupt();
			} catch(IllegalThreadStateException ignored) {
			}
		}
	}
	
	public void setXBoxDeadbands(boolean driver) {
		if(driver) {
			getDriverController().setTriggerDeadbands(0.08, 0.08);
			getDriverController().setLeftStickXDeaband(0.09);
		} else {
			getOperatorController().setLeftStickXDeaband(0.09).setLeftStickYDeadband(0.02)
					.setRightStickXDeadband(0.2).setRightStickYDeadband(0.02);
		}
	}
	
	public void setDualshockDeadbands(boolean driver) {
		var controller = driver ? getDriverController() : getOperatorController();
		controller.setLeftStickXDeaband(0.06).setLeftStickYDeadband(0.03).setRightStickXDeadband(0.05).setRightStickYDeadband(0.03);
	}
}

/*
------------- Results -------------
Deadbands:
	Left X: 0.05291909538209438
	Left Y: 0.02943510375916958
	Right X: 0.04115421138703823
	Right Y: 0.02943510375916958
	Left Trigger: 0.0
	Right Trigger: 0.0
___________________________________
*/
