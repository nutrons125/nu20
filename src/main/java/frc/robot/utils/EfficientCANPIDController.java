package frc.robot.utils;

import com.revrobotics.CANError;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;

/**
 * Reduces CAN usage withing {@link CANPIDController}s
 */
public class EfficientCANPIDController extends CANPIDController {
	private double localReference = 0, localP = 0, localI = 0, localD = 0, localF = 0, localIZone = 0;
	private ControlType localControlType;
	private CANError localCANError;
	
	/**
	 * Constructs a CANPIDController.
	 *
	 * @param device The Spark Max this object configures.
	 */
	public EfficientCANPIDController(CANSparkMax device) {
		super(device);
	}
	
	/**
	 * Set the Proportional Gain constant of the PIDF controller on the SPARK
	 * MAX. This uses the Set Parameter API and should be used infrequently. The
	 * parameter does not presist unless burnFlash() is called.  The recommended
	 * method to configure this parameter is use to SPARK MAX GUI to tune and
	 * save parameters.
	 *
	 * @param gain The proportional gain value, must be positive
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setP(double gain) {
		if(localP != gain)
			return localCANError = super.setP(localP = gain);
		return localCANError;
	}
	
	/**
	 * Set the Integral Gain constant of the PIDF controller on the SPARK MAX.
	 * This uses the Set Parameter API and should be used infrequently. The
	 * parameter does not presist unless burnFlash() is called.  The recommended
	 * method to configure this parameter is use to SPARK MAX GUI to tune and
	 * save parameters.
	 *
	 * @param gain The integral gain value, must be positive
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setI(double gain) {
		if(localI != gain)
			return localCANError = super.setI(localI = gain);
		return localCANError;
	}
	
	/**
	 * Set the Derivative Gain constant of the PIDF controller on the SPARK MAX.
	 * This uses the Set Parameter API and should be used infrequently. The
	 * parameter does not presist unless burnFlash() is called.  The recommended
	 * method to configure this parameter is use to SPARK MAX GUI to tune and
	 * save parameters.
	 *
	 * @param gain The derivative gain value, must be positive
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setD(double gain) {
		if(localD != gain)
			return localCANError = super.setD(localD = gain);
		return localCANError;
	}
	
	/**
	 * Set the Feed-froward Gain constant of the PIDF controller on the SPARK
	 * MAX. This uses the Set Parameter API and should be used infrequently. The
	 * parameter does not presist unless burnFlash() is called.  The recommended
	 * method to configure this parameter is use to SPARK MAX GUI to tune and
	 * save parameters.
	 *
	 * @param gain The feed-forward gain value
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setFF(double gain) {
		if(localF != gain)
			return localCANError = super.setFF(localF = gain);
		return localCANError;
	}
	
	/**
	 * Set the IZone range of the PIDF controller on the SPARK MAX. This value
	 * specifies the range the |error| must be within for the integral constant
	 * to take effect.
	 * <p>
	 * This uses the Set Parameter API and should be used infrequently.
	 * The parameter does not presist unless burnFlash() is called.
	 * The recommended method to configure this parameter is to use the
	 * SPARK MAX GUI to tune and save parameters.
	 *
	 * @param iZone The IZone value, must be positive. Set to 0 to disable
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setIZone(double iZone) {
		if(localIZone != iZone)
			return localCANError = super.setIZone(localIZone = iZone);
		return localCANError;
	}
	
	/**
	 * Set the controller reference value based on the selected control mode.
	 *
	 * @param reference   The value to set depending on the control mode. For basic
	 *                    duty cycle control this should be a value between -1 and 1
	 *                    Otherwise: Voltage Control: Voltage (volts) Velocity Control: Velocity
	 *                    (RPM) Position Control: Position (Rotations) Current Control: Current
	 *                    (Amps). Native units can be changed using the setPositionConversionFactor()
	 *                    or setVelocityConversionFactor() methods of the CANEncoder class
	 * @param controlType Is the control type
	 * @return CANError Set to REV_OK if successful
	 */
	@Override
	public CANError setReference(double reference, ControlType controlType) {
		if(localReference != reference || localControlType != controlType)
			return localCANError = super.setReference(localReference = reference, localControlType = controlType);
		return localCANError;
	}
}
