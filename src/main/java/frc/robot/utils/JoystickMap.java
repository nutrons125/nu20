package frc.robot.utils;
@SuppressWarnings("unused")
public class JoystickMap {
    // Logitech Controller
    public static int A = 1;
	public static int B = 2;
	public static int X = 3;
	public static int Y = 4;
	public static int LB = 5;
	public static int RB = 6;
	public static int BACK = 7;
	public static int START = 8;
	public static int L3 = 9;
	public static int R3 = 10;
	public static int LEFT_TRIGGER = 2;
	public static int RIGHT_TRIGGER = 3;
	public static int LEFT_X = 0;
	public static int LEFT_Y = 1;
	public static int RIGHT_X = 4;
	public static int RIGHT_Y = 5;
}
