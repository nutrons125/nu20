package frc.robot.utils;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartString {
	private final String key;
	private String defaultString = "SmartString default value for %s was never set";
	
	public SmartString(String key) {
		this(key, null);
	}
	
	public SmartString(String key, String initialValue) {
		this(key, initialValue, null);
	}
	
	public SmartString(String key, String initialValue, String defaultValue) {
		this.key = key == null ? "SmartString key is null" : key;
		
		if(defaultValue != null)
			setDefaultValue(defaultValue);
		
		if(initialValue != null)
			setValue(initialValue);
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultString = defaultValue != null ? defaultValue : this.defaultString;
	}
	
	public void setValue(String value) {
		SmartDashboard.putString(key, value);
	}
	
	public String getValue() {
		String defVal;
		
		if(defaultString.contains("%s"))
			defVal = String.format(defaultString, key);
		else {
			defVal = defaultString;
		}
		
		return SmartDashboard.getString(key, defVal);
	}
}
