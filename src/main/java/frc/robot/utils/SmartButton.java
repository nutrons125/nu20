package frc.robot.utils;

import edu.wpi.first.networktables.EntryListenerFlags;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.TableEntryListener;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartButton {
    private final String fieldName;
    private volatile Runnable action;
    private final TableEntryListener entryListener;

    {
        entryListener = (table, key, entry, value, flags) -> {
            if (value.getBoolean()) {
                action.run();
                entry.setBoolean(false);
            }
        };
    }

    public SmartButton(String fieldName) {
        this(fieldName, null);
    }

    public SmartButton(String fieldName, Runnable r) {
        this.action = r;
        this.fieldName = fieldName;
        if (action != null) {
            NetworkTableInstance.getDefault().getTable("SmartDashboard").addEntryListener(entryListener, EntryListenerFlags.kUpdate);
        }
    }

    public boolean get() {
        return SmartDashboard.getBoolean(fieldName, false);
    }

    public void setAction(Runnable r) {
        this.action = r;
        if (action != null) {
            NetworkTableInstance.getDefault().getTable("SmartDashboard").addEntryListener(entryListener, EntryListenerFlags.kUpdate);
        }
    }
}
