package frc.robot.utils;

import java.util.ArrayDeque;

/**
 * This class creates an instance of a moving average.
 * <p>
 * ToDo: Calculate median
 */
public class MovingAverage {
	private int samples;
	
	private ArrayDeque<Double> queue = new ArrayDeque<>();
	private double accumulator = 0;
	
	/**
	 * Constructor for the moving average.
	 *
	 * @param windowSeconds is the number of seconds averaging over
	 */
	public MovingAverage(double windowSeconds) {
		this.samples = (int) Math.ceil(windowSeconds / 0.02);
		
	}
	
	/**
	 * Updates the moving average with the current value of what's being averaged.
	 *
	 * @param value The value to be added to the queue.
	 */
	public MovingAverage update(double value) {
		this.queue.add(value);
		this.accumulator += value;
		if(queue.size() > samples)
			this.accumulator -= this.queue.remove();
		return this;
	}
	
	/**
	 * Calculates the moving average.
	 *
	 * @return The current moving average
	 */
	public double get() {
		return this.accumulator / (double) samples;
	}
	
	public double getMedian() {
		try {
			return (double) queue.toArray()[queue.size() / 2];
		} catch(Exception e) {
			return 0;
		}
	}
	
	public double getQueueSize() {
		return queue.size();
	}
	
	public void preFill() {
		for(int i = 0; i < getQueueSize(); i++) {
			update(0);
		}
	}
}
