package frc.robot.utils;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

/**
 * Class for reducing CAN bus usage among Falcons
 */
public class EfficientFalcon extends TalonFX {
	private ErrorCode localErrorCode = ErrorCode.OK;
	private double localP = 0, localI = 0, localD = 0, localFF = 0;
	private int localIZone = 0;
	
	/**
	 * Constructor
	 *
	 * @param deviceNumber [0,62]
	 */
	public EfficientFalcon(int deviceNumber) {
		super(deviceNumber);
	}
	
	private double kConversionFeetPerTick;
	private double kConversionFeetPerSecondPerTickPer100ms;
	
	public void setPositionConversionFactor(double factor) {
		kConversionFeetPerTick = factor;
	}
	
	public void setVelConversionFactor(double factor) {
		kConversionFeetPerSecondPerTickPer100ms = factor;
	}
	
	public double getVelocity() {
		return this.getSensorCollection().getIntegratedSensorVelocity() * kConversionFeetPerSecondPerTickPer100ms;
	}
	
	
	public double getPosition() {
		return this.getSensorCollection().getIntegratedSensorPosition() * kConversionFeetPerTick;
	}
	
	/**
	 * Sets the appropriate output on the talon, depending on the mode.
	 *
	 * @param mode  The output mode to apply.
	 *              In PercentOutput, the output is between -1.0 and 1.0, with 0.0 as stopped.
	 *              In Current mode, output value is in amperes.
	 *              In Velocity mode, output value is in position change / 100ms.
	 *              In Position mode, output value is in encoder ticks or an analog value,
	 *              depending on the sensor.
	 *              In Follower mode, the output value is the integer device ID of the talon to
	 *              duplicate.
	 * @param value The setpoint value, as described above.
	 *              <p>
	 *              <p>
	 *              Standard Driving Example:
	 *              _talonLeft.set(ControlMode.PercentOutput, leftJoy);
	 */
	@Override
	public void set(TalonFXControlMode mode, double value) {
			super.set(mode, value);
	}
	
	/**
	 * @param mode        Sets the appropriate output on the talon, depending on the mode.
	 * @param demand0     The output value to apply.
	 *                    such as advanced feed forward and/or auxiliary close-looping in firmware.
	 *                    In PercentOutput, the output is between -1.0 and 1.0, with 0.0 as stopped.
	 *                    In Current mode, output value is in amperes.
	 *                    In Velocity mode, output value is in position change / 100ms.
	 *                    In Position mode, output value is in encoder ticks or an analog value,
	 *                    depending on the sensor. See
	 *                    In Follower mode, the output value is the integer device ID of the talon to
	 *                    duplicate.
	 * @param demand1Type The demand type for demand1.
	 *                    Neutral: Ignore demand1 and apply no change to the demand0 output.
	 *                    AuxPID: Use demand1 to set the target for the auxiliary PID 1.  Auxiliary
	 *                    PID is always executed as standard Position PID control.
	 *                    ArbitraryFeedForward: Use demand1 as an arbitrary additive value to the
	 *                    demand0 output.  In PercentOutput the demand0 output is the motor output,
	 *                    and in closed-loop modes the demand0 output is the output of PID0.
	 * @param demand1     Supplmental output value.
	 *                    AuxPID: Target position in Sensor Units
	 *                    ArbitraryFeedForward: Percent Output between -1.0 and 1.0
	 *                    <p>
	 *                    <p>
	 *                    Arcade Drive Example:
	 *                    _talonLeft.set(ControlMode.PercentOutput, joyForward, DemandType.ArbitraryFeedForward, +joyTurn);
	 *                    _talonRght.set(ControlMode.PercentOutput, joyForward, DemandType.ArbitraryFeedForward, -joyTurn);
	 *                    <p>
	 *                    Drive Straight Example:
	 *                    Note: Selected Sensor Configuration is necessary for both PID0 and PID1.
	 *                    _talonLeft.follow(_talonRght, FollwerType.AuxOutput1);
	 *                    _talonRght.set(ControlMode.PercentOutput, joyForward, DemandType.AuxPID, desiredRobotHeading);
	 *                    <p>
	 *                    Drive Straight to a Distance Example:
	 *                    Note: Other configurations (sensor selection, PID gains, etc.) need to be set.
	 *                    _talonLeft.follow(_talonRght, FollwerType.AuxOutput1);
	 */
	@Override
	public void set(TalonFXControlMode mode, double demand0, DemandType demand1Type, double demand1) {
		super.set(mode, demand0, demand1Type, demand1);
	}
	
	/**
	 * Sets the 'P' constant in the given parameter slot.
	 * This is multiplied by closed loop error in sensor units.
	 * Note the closed loop output interprets a final value of 1023 as full output.
	 * So use a gain of '0.25' to get full output if err is 4096u (Mag Encoder 1 rotation)
	 *
	 * @param slotIdx Parameter slot for the constant.
	 * @param value   Value of the P constant.
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	@Override
	public ErrorCode config_kP(int slotIdx, double value) {
		if(localP != value)
			return this.localErrorCode = super.config_kP(slotIdx, localP = value);
		return this.localErrorCode;
	}
	
	/**
	 * Sets the 'I' constant in the given parameter slot.
	 * This is multiplied by accumulated closed loop error in sensor units every PID Loop.
	 * Note the closed loop output interprets a final value of 1023 as full output.
	 * So use a gain of '0.00025' to get full output if err is 4096u for 1000 loops (accumulater holds 4,096,000),
	 * [which is equivalent to one CTRE mag encoder rotation for 1000 milliseconds].
	 *
	 * @param slotIdx Parameter slot for the constant.
	 * @param value   Value of the I constant.
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	@Override
	public ErrorCode config_kI(int slotIdx, double value) {
		if(localI != value)
			return this.localErrorCode = super.config_kI(slotIdx, localI = value);
		return this.localErrorCode;
	}
	
	/**
	 * Sets the 'D' constant in the given parameter slot.
	 * <p>
	 * This is multiplied by derivative error (sensor units per PID loop, typically 1ms).
	 * Note the closed loop output interprets a final value of 1023 as full output.
	 * So use a gain of '250' to get full output if derr is 4096u (Mag Encoder 1 rotation) per 1000 loops (typ 1 sec)
	 *
	 * @param slotIdx Parameter slot for the constant.
	 * @param value   Value of the D constant.
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	@Override
	public ErrorCode config_kD(int slotIdx, double value) {
		if(localD != value)
			return this.localErrorCode = super.config_kD(slotIdx, localD = value);
		return this.localErrorCode;
	}
	
	/**
	 * Sets the 'F' constant in the given parameter slot.
	 * <p>
	 * See documentation for calculation details.
	 * If using velocity, motion magic, or motion profile,
	 * use (1023 * duty-cycle / sensor-velocity-sensor-units-per-100ms).
	 *
	 * @param slotIdx Parameter slot for the constant.
	 * @param value   Value of the F constant.
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	@Override
	public ErrorCode config_kF(int slotIdx, double value) {
		if(localFF != value)
			return this.localErrorCode = super.config_kF(slotIdx, localFF = value);
		return this.localErrorCode;
	}
	
	/**
	 * Sets the Integral Zone constant in the given parameter slot. If the
	 * (absolute) closed-loop error is outside of this zone, integral
	 * accumulator is automatically cleared. This ensures than integral wind up
	 * events will stop after the sensor gets far enough from its target.
	 *
	 * @param slotIdx Parameter slot for the constant.
	 * @param izone   Value of the Integral Zone constant (closed loop error units X
	 *                1ms).
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	@Override
	public ErrorCode config_IntegralZone(int slotIdx, int izone) {
		if(localIZone != izone)
			return this.localErrorCode = super.config_IntegralZone(slotIdx, localIZone = izone);
		return this.localErrorCode;
	}
}
