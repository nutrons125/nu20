package frc.robot.utils;

import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;

/**
 * Reduces CAN signal usage in the CAN bus
 */
public class EfficientSparkMax extends CANSparkMax {
	private double localSpeed = 0, localVolts = 0, localVelocityReference = 0, localDutyReference = 0;
	private EfficientCANPIDController efficientPIDController;
	
	/**
	 * Create a new SPARK MAX Controller
	 *
	 * @param deviceID The device ID.
	 * @param type     The motor type connected to the controller. Brushless motors
	 *                 must be connected to their matching color and the hall sensor
	 *                 plugged in. Brushed motors must be connected to the Red and
	 */
	public EfficientSparkMax(int deviceID, MotorType type) {
		super(deviceID, type);
	}
	
	/**
	 * Common interface for setting the speed of a speed controller.
	 *
	 * @param speed The speed to set. Value should be between -1.0 and 1.0.
	 */
	@Override
	public void set(double speed) {
		if(speed != localSpeed)
			super.set(localSpeed = speed);
	}
	
	/**
	 * Sets the voltage output of the SpeedController.  This is equivillant to
	 * a call to SetReference(output, rev::ControlType::kVoltage). The behavior
	 * of this call differs slightly from the WPILib documetation for this call
	 * since the device internally sets the desired voltage (not a compensation value).
	 * That means that this *can* be a 'set-and-forget' call.
	 *
	 * @param outputVolts The voltage to output.
	 */
	@Override
	public void setVoltage(double outputVolts) {
		if(localVolts != outputVolts)
			super.setVoltage(localVolts = outputVolts);
	}
	
	/**
	 * Sets the velocity reference for this motor controller
	 *
	 * @param velocity Velocity reference
	 */
	public void setVelocity(double velocity) {
		if(localVelocityReference != velocity)
			getPIDController().setReference(localVelocityReference = velocity, ControlType.kVelocity);
	}
	
	/**
	 * Gets a {@link EfficientCANPIDController} from this class
	 *
	 * @return An object for interfacing with the integrated PID controller.
	 */
	@Override
	public CANPIDController getPIDController() {
		return efficientPIDController = efficientPIDController == null
				? new EfficientCANPIDController(this) : efficientPIDController;
	}
	
	/**
	 * Gets a {@link EfficientCANPIDController} from this class
	 * <p>
	 * Removes the need for casting
	 * </p>
	 *
	 * @return An object for interfacing with the integrated PID controller.
	 */
	public EfficientCANPIDController getEfficientPIDController() {
		return (EfficientCANPIDController) getPIDController();
	}
}
