package frc.robot.utils;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import frc.robot.subsystems.Drivetrain;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.util.ArrayDeque;

/**
 * Class to represent a Limelight with
 */
@SuppressWarnings("unused")
public class Limelight {
	
	/**
	 * Sets the LED mode of this Limelight
	 */
	public enum LEDMode {
		Pipeline(0), Off(1), Blink(2), On(3);
		private int mode;
		
		LEDMode(int mode) {
			this.mode = mode;
		}
		
		private int getMode() {
			return mode;
		}
	}
	
	/**
	 * Sets the camera to function as a regular camera or a vision processor
	 */
	public enum CameraMode {
		VisionProcessor(0), DriverCamera(1);
		private int mode;
		
		CameraMode(int mode) {
			this.mode = mode;
		}
		
		private int getMode() {
			return mode;
		}
	}
	
	/**
	 * Sets the camera stream layout
	 * <p>
	 * <li>Standard - Processor | USB Camera View</li>
	 * <li>PiPMain - Processor | Small USB Camera View</li>
	 * <li>PiPMain - USB Camera | Small Processor View</li>
	 * </p>
	 */
	public enum StreamMode {
		Standard(0), PiPMain(1), PiPSecondary(2);
		private int mode;
		
		StreamMode(int mode) {
			this.mode = mode;
		}
		
		private int getMode() {
			return mode;
		}
	}
	
	/**
	 * Sets the Limelight snapshot mode
	 */
	public enum Snapshot {
		NoSnapshots(0), TwoASecond(1);
		private int mode;
		
		Snapshot(int mode) {
			this.mode = mode;
		}
		
		private int getMode() {
			return mode;
		}
	}
	
	private NetworkTableEntry tv, tx, ty, ta, ts, tl, tShort, tLong, tHor, tVert, getPipe, camTran,
			ledMode, camMode, pipeline, stream, snapshot, tCornX, tCornY;
	private double tvValue, txValue, tyValue, taValue, tsValue, tlValue, tShortValue, tLongValue, tHorValue, tVertValue,
			getPipeValue;
	private double filteredX = 0;
	private double[] tCornXValue, tCornYValue, camTranValue;
	
	private static final double X_FILTER_ORDER = 101,
			Y_FILTER_ORDER = 101;
	private double[] xValueSamples = new double[(int) X_FILTER_ORDER];
	private double[] yValueSamples = new double[(int) Y_FILTER_ORDER];
	
	private ArrayDeque<Double> xOffSamples = new ArrayDeque<>(),
			xOutputs = new ArrayDeque<>(),
			yOffSamples = new ArrayDeque<>();
	
	private LEDMode localLEDMode = LEDMode.Pipeline;
	private CameraMode localCameraMode = CameraMode.VisionProcessor;
	private StreamMode localStreamMode = StreamMode.Standard;
	private Snapshot localSnapshotMode = Snapshot.NoSnapshots;
	private int localPipeline = 0;
	
	public Limelight(String address) {
		
		NetworkTable table = NetworkTableInstance.getDefault().getTable(address);
		tv = table.getEntry("tv");
		tx = table.getEntry("tx");
		ty = table.getEntry("ty");
		ta = table.getEntry("ta");
		ts = table.getEntry("ts");
		tl = table.getEntry("tl");
		tShort = table.getEntry("tshort");
		tLong = table.getEntry("tlong");
		tHor = table.getEntry("thor");
		tVert = table.getEntry("tvert");
		getPipe = table.getEntry("getpipe");
		camTran = table.getEntry("camtran");
		ledMode = table.getEntry("ledMode");
		camMode = table.getEntry("camMode");
		pipeline = table.getEntry("pipeline");
		stream = table.getEntry("stream");
		snapshot = table.getEntry("snapsot");
		tCornX = table.getEntry("tcornx");
		tCornY = table.getEntry("tcorny");
		update();
	}
	
	/**
	 * Updates only v, x, and y
	 */
	public void updateXY() {
		tvValue = tv.getDouble(0);
		txValue = tx.getDouble(0);
		tyValue = ty.getDouble(0);
	}
	
	/**
	 * Updates all of this classes variables with what the Limelight is outputting
	 */
	public void update() {
		tvValue = tv.getDouble(0);
		txValue = tx.getDouble(0);
		tyValue = ty.getDouble(0);
		taValue = ta.getDouble(0);
		tsValue = ts.getDouble(0);
		tlValue = tl.getDouble(0);
		tShortValue = tShort.getDouble(0);
		tLongValue = tLong.getDouble(0);
		tHorValue = tHor.getDouble(0);
		tVertValue = tVert.getDouble(0);
		getPipeValue = getPipe.getDouble(0);
		camTranValue = camTran.getDoubleArray(new double[6]);
		tCornXValue = tCornX.getDoubleArray(new double[4]);
		tCornYValue = tCornY.getDoubleArray(new double[4]);
	}
	
	/**
	 * Tells if a target is detected in the vision processing
	 *
	 * @return Target found
	 */
	public boolean targetFound() {
		return tvValue == 1;
	}
	
	/**
	 * Gets the X angle offset from the crosshair
	 *
	 * @return X angle offset from target
	 */
	public double getXOffset() {
		return txValue;
	}
	
	/**
	 * Gets the filtered X angle offset from the crosshair
	 * <p>
	 * Uses notch filter in standard form found <a href="https://wikimedia.org/api/rest_v1/media/math/render/svg/8a6c0c074f3a4cf20037523deda9fdedfe614fce">here</a>
	 * </p>
	 *
	 * @return filtered X offset
	 */
	public double getFilteredXOffset() {
		return filteredX;
	}
	
	/**
	 * Gets the Y angle offset from the crosshair
	 *
	 * @return Y angle offset from target
	 */
	public double getYOffset() {
		return tyValue;
	}
	
	/**
	 * Gets the target box area
	 *
	 * @return Target box area
	 */
	public double getArea() {
		return taValue;
	}
	
	/**
	 * Gets the skew of the target
	 *
	 * @return Target skew
	 */
	public double getSkew() {
		return tsValue;
	}
	
	/**
	 * Gets the latency from the Limelight to radio or whatever
	 *
	 * @return Latency
	 */
	public double getLatency() {
		return tlValue;
	}
	
	/**
	 * Gets the shortest side length of the target box
	 *
	 * @return Shortest box sidelength
	 */
	public double getShortestSidelength() {
		return tShortValue;
	}
	
	/**
	 * Gets the longest side length of the target box
	 *
	 * @return Longest box sidelength
	 */
	public double getLongestSidelength() {
		return tLongValue;
	}
	
	/**
	 * Gets the horizontal side length of the target box
	 *
	 * @return Horizontal side length
	 */
	public double getHorizontalSidelength() {
		return tHorValue;
	}
	
	/**
	 * Gets the vertical side length of the target box
	 *
	 * @return Vertical side length
	 */
	public double getVerticalSidelength() {
		return tVertValue;
	}
	
	/**
	 * Gets the selected pipeline that the Limelight is running
	 *
	 * @return Selected pipeline
	 */
	public double getPipeline() {
		return getPipeValue;
	}
	
	/**
	 * Gets the "cam tran"
	 *
	 * @return Yup
	 * @see <a href="http://www.camtranbus.com/">camtranbus.com</a>
	 */
	public double[] getCamTran() {
		return camTranValue;
	}
	
	/**
	 * Gets the pose relative to the vision target. This only works with 3D enabled.
	 *
	 * @return Pose relative to vision target;
	 */
	public Pose2d getCamTranPose() {
		return camTranValue[0] == 0 && camTranValue[4] == 0
				? Drivetrain.getInstance().getCurrentPose()
				: new Pose2d(new Translation2d(camTranValue[0] / 12, camTranValue[2] / -12), Rotation2d.fromDegrees(camTranValue[4]));
	}
	
	/**
	 * Gets the target box's x corner coordinates
	 *
	 * @return X Corner coordinates
	 */
	public double[] getCornerXCoords() {
		return tCornXValue;
	}
	
	/**
	 * Gets the target box's y corner coordinates
	 *
	 * @return Y Corner coordinates
	 */
	public double[] getCornerYCoords() {
		return tCornYValue;
	}
	
	/**
	 * Sets the LED mode of the Limelight
	 *
	 * @param mode LEDMode to set
	 */
	public void setLEDMode(LEDMode mode) {
		if(localLEDMode != mode) {
			localLEDMode = mode;
			ledMode.setDouble(mode.getMode());
		}
	}
	
	/**
	 * Sets the camera mode of the Limelight
	 *
	 * @param mode Camera mode to set
	 */
	public void setCamMode(CameraMode mode) {
		if(localCameraMode != mode) {
			localCameraMode = mode;
			camMode.setDouble(mode.getMode());
		}
	}
	
	/**
	 * Sets the Limelight's pipeline
	 *
	 * @param pipelineNum User created pipeline number
	 */
	public void setPipeline(int pipelineNum) {
		if(localPipeline != pipelineNum)
			pipeline.setDouble(localPipeline = pipelineNum);
	}
	
	/**
	 * Sets the Limelight's stream mode
	 *
	 * @param mode Stream mode to feed back to the Driver Station
	 * @see StreamMode
	 */
	public void setStreamMode(StreamMode mode) {
		if(localStreamMode != mode) {
			localStreamMode = mode;
			stream.setDouble(mode.getMode());
		}
	}
	
	/**
	 * Sets the Limelight's snapshot mode
	 *
	 * @param mode Snapshot mode to set
	 */
	public void setSnapshotMode(Snapshot mode) {
		if(localSnapshotMode != mode) {
			localSnapshotMode = mode;
			snapshot.setDouble(mode.getMode());
		}
	}
	
	/**
	 * Gets a 1x2 matrix of x and y offsets
	 *
	 * @return {@link Mat} of x and y offsets
	 */
	public Mat getCoordMatrix() {
		Mat mat = new Mat(1, 2, CvType.CV_64F);
		mat.put(1, 1, getXOffset());
		mat.put(1, 2, getYOffset());
		return mat;
	}
	
	public Mat getCornerMatrix() {
		return new Mat();
	}
}
