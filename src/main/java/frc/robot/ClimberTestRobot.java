/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Intake;

public class ClimberTestRobot extends TimedRobot {
    private Climber climber;

    @Override
    public void robotInit() {
        climber = Climber.getInstance();
        climber.robotInit();

        OI.getInstance().getOperatorController().setLeftStickYDeadband(0.08);
    }

    @Override
    public void robotPeriodic() {
    }

    @Override
    public void autonomousInit() {
    }

    @Override
    public void autonomousPeriodic() {
    }

    @Override
    public void teleopInit() {
    }

    @Override
    public void teleopPeriodic() {
        if (OI.getInstance().getOperatorController().getDPadUp()) {
            climber.setClimbState(Climber.ClimbStates.MANUALUP);
        } else if (climber.getClimbState()== Climber.ClimbStates.MANUALUP) {
            climber.setClimbState(Climber.ClimbStates.DEFAULT);
        } else if (OI.getInstance().getOperatorController().getDPadDown()) {
            climber.setClimbState(Climber.ClimbStates.MANUALDOWN);
        } else if (climber.getClimbState()== Climber.ClimbStates.MANUALDOWN) {
            climber.setClimbState(Climber.ClimbStates.DEFAULT);
        } else if (OI.getInstance().getOperatorController().y()) {
            climber.setClimbState(Climber.ClimbStates.PRECLIMB);
        } else if (OI.getInstance().getOperatorController().a()) {
            climber.setClimbState(Climber.ClimbStates.GRAB);
        }
        climber.runTeleOp();

        //if d-pad up
        // set climb state to manual up
        //
        // else if d-pad down,
        // set state to manual down

        //else if extending, set state to preclimb

        //else if button for retracting, set state to grab

        //else if


    }

    public void testPeriodic() {
        climber.testControl(OI.getInstance().getOperatorController().getLeftStickY(),
                OI.getInstance().getOperatorController().a());
    }

    @Override
    public void disabledInit() {
    }

    @Override
    public void disabledPeriodic() {
    }

    public void updateShuffleboard() {
    }
}
