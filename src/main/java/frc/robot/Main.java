package frc.robot;

import edu.wpi.first.wpilibj.RobotBase;
import frc.robot.testrobots.HoodTestRobot;

public final class Main {
	
	private Main() {
	}
	
	public static void main(String... cheese) {
		RobotBase.startRobot(Robot::new);
	}
}
