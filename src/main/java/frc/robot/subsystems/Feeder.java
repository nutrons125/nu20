package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.logging.Loggable;

/**
 * Feeder Class
 */
public class Feeder extends SubsystemBase implements Loggable {
	private static Feeder instance;
	public static final double BALL_FILL_POWER = 0.4, FEED_SHOOTER_POWER = .75;
	CANSparkMax feederSpinner;

	/**
	 * Constructs a new Feeder object
	 */
	private Feeder() {
		feederSpinner = new CANSparkMax(RobotMap.FEEDER_SPINNER, MotorType.kBrushless);
		feederSpinner.restoreFactoryDefaults();
		feederSpinner.setIdleMode(CANSparkMax.IdleMode.kCoast);
		feederSpinner.setInverted(true);
		feederSpinner.setSmartCurrentLimit(30);
		feederSpinner.burnFlash();
	}

	public static Feeder getInstance() {
		return instance = instance == null ? new Feeder() : instance;
	}

	/**
	 * Code For the Tele-OP Period
	 */
	public void runB(boolean run) {
		if (run) {
			feederSpinner.set(1);
		} else {
			feederSpinner.set(0);
		}
	}

	public void run(double power) {
//		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
//		StringBuilder calls = new StringBuilder();
//		calls.append("Line Num: ").append(stackTrace[1].getLineNumber()).append(" ");
//		for (StackTraceElement element : stackTrace) {
//			calls.append(element.getClassName().substring(element.getClassName().lastIndexOf(".") + 1)).append(".").append(element.getMethodName()).append(":").append(element.getLineNumber()).append(" <- ");
//		}
//		SmartDashboard.putString("Feeder run() stack trace", calls.toString());
		feederSpinner.set(power);
	}

	public double getOutputCurrent() {
		return feederSpinner.getOutputCurrent();
	}

	@Override
	public String[] setCSVHeader() {
		return new String[]{"Feeder Speed"};
	}


	@Override
	public String[] toCSVData() {
		return new String[]{feederSpinner.get() + ""};
	}

}