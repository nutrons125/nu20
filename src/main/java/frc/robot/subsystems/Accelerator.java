package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.TalonSRXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.robot.RobotMap;

public class Accelerator {
    private static Accelerator instance;
    public static final double FEED_SHOOTER_POWER = 1, FILL_ACCELERATOR_POWER = 0.3;
    TalonSRX acceleratorMotors;

    private Accelerator() {
        acceleratorMotors = new TalonSRX(RobotMap.ACCELERATOR);
        acceleratorMotors.configSelectedFeedbackSensor(TalonSRXFeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
        acceleratorMotors.configContinuousCurrentLimit(25);
        acceleratorMotors.enableCurrentLimit(true);
        acceleratorMotors.setInverted(true);
    }

    public static Accelerator getInstance() {
        return instance = instance == null ? new Accelerator() : instance;
    }

    public void run(double power) {
        acceleratorMotors.set(ControlMode.PercentOutput, -power);
    }

    public double getEncoderPosition() {
        return acceleratorMotors.getSelectedSensorPosition();
    }
}
