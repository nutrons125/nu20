package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.*;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.OI;
import frc.robot.RobotMap;
import frc.robot.logging.Loggable;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.EfficentFalcon;
import frc.robot.utils.MovingAverage;

public class Drivetrain extends SubsystemBase implements Loggable {
	private static Drivetrain instance;
	private static final double ALIGN_P = 0.012, ALIGN_D = 0.000;
	private static final double THROTTLE_SCALE = 1, H_MAX_TURN = .4, L_MAX_TURN = .2;
	private double previousError = 0, previousTime = 0;
	private MovingAverage alignMovingAverage = new MovingAverage(0.1);
	
	
	public static Drivetrain getInstance() {
		return instance = instance == null ? new Drivetrain() : instance;
	}
	
	@Override
	public String[] setCSVHeader() {
		return new String[0];
	}
	
	@Override
	public String[] toCSVData() {
		return new String[0];
	}
	
	//k constants
	/**
	 * Note to Self:
	 * I already changed the wheel diameter, ratio, and max linear speed. We need to characterize to get the rest of the k values.
	 * <p>
	 * TODO:
	 * add gyro back into code?
	 * Figure out can IDs and directions.
	 * Start paths?
	 * Figure out a more efficient way to switch between drive types(maybe assign drive changing button).
	 * set motors to coast mode
	 * <p>
	 * stuff to remember:
	 * 2048 units in one rotation
	 * speed is in units per 100 ms(0.1 seconds)
	 */
	
	private final double kDriveTrainGearRatio = 8.0;
	private final double kMaxAccel = 14.2;
	private final double kMinAccel = -kMaxAccel;
	private final double kWheelDiameterFeet = 0.4166;
	private final double ksVolts = 0.194;
	public final double kMaxLinearSpeedFeetPerSecond = 14.2;
	private final double kvVoltsSecondPerFeet = 0.652;
	private final double kaVoltsSecondSquaredPerFeet = 0.0796;
	private static double kTrackwidthFeet = 4.5;
	//private static final double kpVel;
	private Trajectory currentPath;
	private RamseteController pathController = new RamseteController(0.01, 0.9);
	private DifferentialDriveOdometry odometry;
	private AHRS gyro;
	private Timer pathTimer;
	private double lastVelLeft = 0;
	private double lastVelRight = 0;
	public final double kOmegaMaxRadiansPerSec = 2 * kMaxLinearSpeedFeetPerSecond / kTrackwidthFeet;
	
	
	public static enum State {
		TELEOP_DRIVING,
		TEST,
		ALIGNING,
		//TURN_IN_PLACE,
		PATH_FOLLOWING,
		DONE_FOLLOWING,
		DONE_ALIGNING
	}
	
	public static enum TeleopDriveType {
		CHEESY,
		ARCADE
	}
	
	private TeleopDriveType driveType = TeleopDriveType.ARCADE;
	
	private SimpleMotorFeedforward driveMotorFeedforward;
	private EfficentFalcon leftDriveFollower;
	private EfficentFalcon rightDriveMain;
	private EfficentFalcon rightDriveFollower;
	private EfficentFalcon rightDriveFollower2;
	private EfficentFalcon leftDriveFollower2;
	private EfficentFalcon leftDriveMain;
	private DifferentialDriveKinematics kDriveKinematics;
	
	private Drivetrain() {
		// leftDriveMain.restoreFactoryDefaults();
		// leftDriveFollower.restoreFactoryDefaults();
		// rightDriveMain.restoreFactoryDefaults();
		// rightDriveFollower.restoreFactoryDefaults();
		pathTimer = new Timer();
		gyro = new AHRS(SPI.Port.kMXP);
		driveMotorFeedforward = new SimpleMotorFeedforward(ksVolts, kvVoltsSecondPerFeet, kaVoltsSecondSquaredPerFeet);
		odometry = new DifferentialDriveOdometry(Rotation2d.fromDegrees(0));
		kDriveKinematics = new DifferentialDriveKinematics(kTrackwidthFeet);
		leftDriveMain = new EfficentFalcon(RobotMap.LEFT_MAIN);
		leftDriveFollower = new EfficentFalcon(RobotMap.LEFT_FOLLOWER);
		leftDriveFollower2 = new EfficentFalcon (RobotMap.LEFT_FOLLOWER_2);
		rightDriveMain = new EfficentFalcon(RobotMap.RIGHT_MAIN);
		rightDriveFollower = new EfficentFalcon(RobotMap.RIGHT_FOLLOWER);
		rightDriveFollower2 = new EfficentFalcon (RobotMap.RIGHT_FOLLOWER_2);
		
		leftDriveMain.enableVoltageCompensation(true);
		leftDriveMain.configVoltageCompSaturation(12);
		rightDriveMain.enableVoltageCompensation(true);
		rightDriveMain.configVoltageCompSaturation(12);
		leftDriveFollower.enableVoltageCompensation(true);
		leftDriveFollower.configVoltageCompSaturation(12);
		rightDriveFollower.enableVoltageCompensation(true);
		rightDriveFollower.configVoltageCompSaturation(12);
		leftDriveFollower2.enableVoltageCompensation(true);
		leftDriveFollower2.configVoltageCompSaturation(12);
		rightDriveFollower2.enableVoltageCompensation(true);
		rightDriveFollower2.configVoltageCompSaturation(12);
		leftDriveMain.setInverted(true);
		leftDriveFollower.setInverted(true);
		leftDriveFollower2.setInverted(true);
		leftDriveFollower.follow(leftDriveMain);
		leftDriveFollower2.follow(leftDriveMain);
		rightDriveFollower.follow(rightDriveMain);
		rightDriveFollower2.follow(rightDriveMain);
		leftDriveMain.setNeutralMode(NeutralMode.Coast);
		leftDriveFollower.setNeutralMode(NeutralMode.Coast);
		leftDriveFollower2.setNeutralMode(NeutralMode.Coast);
		rightDriveMain.setNeutralMode(NeutralMode.Coast);
		rightDriveFollower.setNeutralMode(NeutralMode.Coast);
		rightDriveFollower2.setNeutralMode(NeutralMode.Coast);
		double ramp = .3;
		leftDriveMain.configOpenloopRamp(ramp);
		leftDriveFollower.configOpenloopRamp(ramp);
		leftDriveFollower2.configOpenloopRamp(ramp);
		rightDriveMain.configOpenloopRamp(ramp);
		rightDriveFollower.configOpenloopRamp(ramp);
		rightDriveFollower2.configOpenloopRamp(ramp);
		leftDriveMain.setPositionConversionFactor(((Math.PI * kWheelDiameterFeet) / kDriveTrainGearRatio) / 2048);
		rightDriveMain.setPositionConversionFactor(((Math.PI * kWheelDiameterFeet) / kDriveTrainGearRatio) / 2048);
		leftDriveMain.setSensorPhase(false);
		leftDriveMain.setSelectedSensorPosition(0);
		leftDriveFollower.setSelectedSensorPosition(0);
		leftDriveFollower2.setSelectedSensorPosition(0);
		rightDriveFollower.setSelectedSensorPosition(0);
		rightDriveFollower2.setSelectedSensorPosition(0);
		rightDriveMain.setSelectedSensorPosition(0);
		
		leftDriveMain.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		leftDriveFollower.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		leftDriveFollower2.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		rightDriveMain.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		rightDriveFollower.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		rightDriveFollower2.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 3));
		
		leftDriveMain.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		leftDriveFollower.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		leftDriveFollower2.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		rightDriveMain.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		rightDriveFollower.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		rightDriveFollower2.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		
		gyro.zeroYaw();
		
		/** conversation factor for vel math: 
		 * speed is originally in units/0.1 sec, so multiply by 10 and divide by 2048 to get gear rotation per sec, then divide by gear ratio and multiply by PI and wheel diameter to get ft per sec. 
		 *
		 **/
		leftDriveMain.setVelConversionFactor((-10 * Math.PI * kWheelDiameterFeet) / (2048 * kDriveTrainGearRatio));
		rightDriveMain.setVelConversionFactor((10 * Math.PI * kWheelDiameterFeet) / (2048 * kDriveTrainGearRatio));
		SmartDashboard.putNumber("Path Linear", 0);
		SmartDashboard.putNumber("Path Angular", 0);
		SmartDashboard.putNumber("FF Path Linear", 0);
		SmartDashboard.putNumber("FF Path Angular", 0);
		
		CommandScheduler.getInstance().registerSubsystem(this);
		SmartDashboard.putNumber("Goal Pose Angle", 0);
	}
	
	public DifferentialDriveKinematics getKinematics() {
		return kDriveKinematics;
	}
	
	public void setBrake() {
		leftDriveMain.setNeutralMode(NeutralMode.Brake);
		leftDriveFollower.setNeutralMode(NeutralMode.Brake);
		leftDriveFollower2.setNeutralMode(NeutralMode.Brake);
		rightDriveMain.setNeutralMode(NeutralMode.Brake);
		rightDriveFollower.setNeutralMode(NeutralMode.Brake);
		rightDriveFollower2.setNeutralMode(NeutralMode.Brake);
	}
	
	private void driveArcade() {
		double linear = OI.getInstance().getDriverController().getTriggerSum() * kMaxLinearSpeedFeetPerSecond;
		double angular = -1 * OI.getInstance().getDriverController().getLeftStickX() * kOmegaMaxRadiansPerSec;
//		double linear = OI.getInstance().getDriverController().getTriggerSum();
//		double angular = OI.getInstance().getDriverController().getLeftStickX();
		driveChassisSpeeds(linear, angular * 0.3);
	}
	
	private void arcadeDrive(double throttle, double turn) {
		leftDriveMain.set(TalonFXControlMode.PercentOutput, throttle + turn);
		rightDriveMain.set(TalonFXControlMode.PercentOutput, throttle - turn);
	}
	
	public void driveNormal(double throttle, double turn) {
		leftDriveMain.set(ControlMode.PercentOutput, throttle + turn);
		rightDriveMain.set(ControlMode.PercentOutput, throttle - turn);
		
	}
	
	/**
	 * For P-Trick <p>
	 * Inverse proportional driving (sort of)
	 *
	 * @param throttle          forward and backward input [-1, 1]
	 * @param turn              left and right input [-1, 1]
	 * @param throttleScale     scales throttle by a given scale factor [>0, 1]
	 * @param lowestMaxTurnCmd  Sets the lowest max turn Cmd. If this is 0.2 and throttle=1 and turn=1 this will multiply the turn by .2
	 * @param highestMaxTurnCmd Sets the highest max turn Cmd. If this is 0.8 and throttle=0 and turn=1 this will multiply the turn by .8
	 */
	public void pTrickDrive(double throttle, double turn, double throttleScale, double highestMaxTurnCmd, double lowestMaxTurnCmd) {
		double turnScale = ((-(highestMaxTurnCmd - lowestMaxTurnCmd)) * Math.abs(throttle)) + highestMaxTurnCmd;
		arcadeDrive(throttle * throttleScale, turn * turnScale);
		//		driveCheesy(throttle * throttleScale, turn * turnScale);
	}
	
	private void driveCheesy() {
		double linear = OI.getInstance().getDriverController().getTriggerSum() * kMaxLinearSpeedFeetPerSecond;
		double curvature = OI.getInstance().getDriverController().getLeftStickX() * 2 * Math.PI / kTrackwidthFeet;
		double omega = -1 * curvature * linear;
		driveChassisSpeeds(linear, omega);
	}
	
	private void driveCheesy(double throttle, double turn) {
		double linear = throttle * kMaxLinearSpeedFeetPerSecond;
		double curvature = turn * 2 * Math.PI / kTrackwidthFeet;
		double omega = -1 * curvature/* * linear*/;
		driveChassisSpeeds(linear, omega);
		
	}
	
	private void driveChassisSpeeds(double linear, double angular) {
		ChassisSpeeds speeds = new ChassisSpeeds(linear, 0, angular);
		DifferentialDriveWheelSpeeds wheelSpeeds = kDriveKinematics.toWheelSpeeds(speeds);
		double velLeft = wheelSpeeds.leftMetersPerSecond;
		double velRight = wheelSpeeds.rightMetersPerSecond;
		driveVelocities(velLeft, velRight);
	}
	
	
	private void driveVelocities(double velLeft, double velRight) {
		double accelLeft = (velLeft - lastVelLeft) / 0.02;
		accelLeft = Math.max(kMinAccel, Math.min(accelLeft, kMaxAccel));
		lastVelLeft = velLeft;
		double leftVolt = driveMotorFeedforward.calculate(velLeft, accelLeft);

		if(velLeft == 0) {
			leftVolt = 0;
		}
		double accelRight = (velRight - lastVelRight) / 0.02;
		accelRight = Math.max(kMinAccel, Math.min(accelRight, kMaxAccel));
		
		lastVelRight = velRight;
		double rightVolt = driveMotorFeedforward.calculate(velRight, accelRight);
		if(velRight == 0) {
			rightVolt = 0;
		}
		leftDriveMain.set(ControlMode.PercentOutput, leftVolt / 12.0);
		rightDriveMain.set(ControlMode.PercentOutput, rightVolt / 12.0);
	}
	
	
	private void alignDrivetrain() {
		var currentTime = Timer.getFPGATimestamp();
		var dt = currentTime - previousTime;
		previousTime = currentTime;
		
		var error = alignMovingAverage.get();
		
		var turn = (ALIGN_P * error) + (ALIGN_D * (error - previousError) / dt);
		previousError = error;
		
		leftDriveMain.set(ControlMode.PercentOutput, turn);
		rightDriveMain.set(ControlMode.PercentOutput, -turn);
	}
	
	public Pose2d getCurrentPose() {
		return odometry.getPoseMeters();
	}
	
	public void setCoast() {
		leftDriveMain.setNeutralMode(NeutralMode.Coast);
		leftDriveFollower.setNeutralMode(NeutralMode.Coast);
		leftDriveFollower2.setNeutralMode(NeutralMode.Coast);
		rightDriveFollower.setNeutralMode(NeutralMode.Coast);
		rightDriveMain.setNeutralMode(NeutralMode.Coast);
		rightDriveFollower2.setNeutralMode(NeutralMode.Coast);
	}
	
	private void followPath() {
		var goalPose = currentPath.sample(pathTimer.get());
		SmartDashboard.putNumber("Goal Pose Angle", goalPose.poseMeters.getRotation().getDegrees());
		SmartDashboard.putString("Goal Pose", goalPose.toString());
		var currentPose = odometry.getPoseMeters();
		var controllerResult = pathController.calculate(currentPose, goalPose);
		var speeds = kDriveKinematics.toWheelSpeeds(controllerResult);
		Pose2d poseError = goalPose.poseMeters.relativeTo(currentPose);
		
		System.out.println(String.format("Goal: %s Pose error %s, controller result %s", goalPose.toString(), poseError.toString(), controllerResult.toString()));
		SmartDashboard.putNumber("Path Linear", controllerResult.vxMetersPerSecond);
		SmartDashboard.putNumber("Path Angular", controllerResult.omegaRadiansPerSecond);
		SmartDashboard.putNumber("FF Path Linear", goalPose.velocityMetersPerSecond);
		SmartDashboard.putNumber("FF Path Angular", goalPose.velocityMetersPerSecond * goalPose.curvatureRadPerMeter);
		
		driveVelocities(speeds.leftMetersPerSecond, speeds.rightMetersPerSecond);
		//driveChassisSpeeds(goalPose.velocityMetersPerSecond, goalPose.velocityMetersPerSecond * goalPose.curvatureRadPerMeter);
		//driveVelocities(0, 0);
		
		// If path is finished, then set state to path done
		if(pathTimer.hasPeriodPassed(currentPath.getTotalTimeSeconds())) {
			state = State.DONE_FOLLOWING;
		}
	}
	
	public double getHeading() {
		return gyro.getPitch();
	}
	
	public double getAngle() {
		return -gyro.getAngle();
	}
	
	private State state = State.TELEOP_DRIVING;
	
	public double getRightDistanceFeet() {
		return rightDriveMain.getSelectedSensorPosition();
	}
	
	public double getLeftDistanceFeet() {
		return leftDriveMain.getSelectedSensorPosition();
	}
	
	public SimpleMotorFeedforward getFeedforward() {
		return driveMotorFeedforward;
	}
	
	public void stopDriving() {
		driveVelocities(0, 0);
	}
	
	public void enterTeleop() {
		state = State.TELEOP_DRIVING;
	}
	
	public void enterTest() {
		state = State.TEST;
	}
	//unfinished, we don't know if it will be used. 
	// public void turnInPlace(double desiredAngle){
	// 	double maxTurn = 9.0;
	// 	double currentAngle = gyro.getAngle();
	// 	double angleError = desiredAngle - currentAngle; 
	// 	double turn = angleError* 0.3;
	// 	if(turn >= maxTurn){
	// 		turn = maxTurn;
	// 	}
	// 	if(turn <= -maxTurn){
	// 		turn = -maxTurn;
	// 	}
	
	
	// 	driveVelocities(-2.0, 2.0);
	// }
	
	
	@Override
	public void periodic() {
		alignMovingAverage.update(Vision.getInstance(1).getLimelight1().getXOffset());
		odometry.update(Rotation2d.fromDegrees(-gyro.getAngle()), getLeftDistanceFeet(), getRightDistanceFeet());
		
		switch(state) {
			case TEST:
				if(OI.getInstance().getDriverController().a()) {
					//driveVelocities(2.0, 2.0);
					driveChassisSpeeds(0, .25 * kOmegaMaxRadiansPerSec);
				} else if(OI.getInstance().getDriverController().y()) {
					driveChassisSpeeds(0, -0.25 * kOmegaMaxRadiansPerSec);
				} else if(OI.getInstance().getDriverController().x()) {
					driveVelocities(2.0, 2.0);
				} else if(OI.getInstance().getDriverController().b()) {
					driveVelocities(-2.0, -2.0);
				} else {
					
					driveArcade();
				}
				break;
			
			case TELEOP_DRIVING:
				switch(driveType) {
					case ARCADE:
/*
					if(OI.getInstance().getDriverController().a()){
						//driveVelocities(2.0, 2.0);
						driveChassisSpeeds(0, .25*kOmegaMaxRadiansPerSec);
					} else if(OI.getInstance().getDriverController().y()){
						driveChassisSpeeds(0, -0.25*kOmegaMaxRadiansPerSec);
					} else{
						driveArcade();
					}
*/
						pTrickDrive(OI.getInstance().getDriverController().getTriggerSum(), OI.getInstance().getDriverController().getLeftStickX(),
								THROTTLE_SCALE, H_MAX_TURN, L_MAX_TURN);
//						arcadeDrive(OI.getInstance().getDriverController().getTriggerSum(), OI.getInstance().getDriverController().getLeftStickX());
						break;
					case CHEESY:
						driveCheesy();
						break;
					
					default:
						break;
				}
				
				break;
			case ALIGNING:
				alignDrivetrain();
				break;
			case PATH_FOLLOWING:
				followPath();
				break;
			// case TURN_IN_PLACE:
			// 	//turnInPlace();
			// 	break;
			case DONE_FOLLOWING:
				driveVelocities(0, 0);
				break;
			default:
				break;
		}
		SmartDashboard.putString("DT STate", state.toString());
		SmartDashboard.putString("Pose", odometry.getPoseMeters().toString());
		
		boolean navXOK = gyro.isConnected();
		SmartDashboard.putBoolean("Nav X Connected", navXOK);
	}
	
	public void resetGyro() {
		gyro.reset();
	}
	
	public void  resetOdometry() {
		gyro.reset();
		leftDriveMain.setSelectedSensorPosition(0);
		rightDriveMain.setSelectedSensorPosition(0);
		odometry.resetPosition(new Pose2d(0, 0, Rotation2d.fromDegrees(0)), Rotation2d.fromDegrees(0));
	}
	
	
	public void startFollowingPath(Trajectory path) {
		currentPath = path;
		pathTimer.reset();
		pathTimer.start();
		state = State.PATH_FOLLOWING;
	}
	
	public void startAligning() {
	
	}
	
	public boolean doneAligning() {
		return state == State.DONE_ALIGNING;
	}
	
	public boolean doneFollowing() {
		return state == State.DONE_FOLLOWING;
		
	}
}
