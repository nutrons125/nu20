package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SendableRegistry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.OI;
import frc.robot.RobotMap;
import frc.robot.commands.*;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.*;

import java.util.ArrayList;
import java.util.List;

public class Superstructure extends SubsystemBase {
	private static final double TURRET_MANUAL_SPIN_SCALE = 20;
	private static Superstructure instance;
	public boolean reverseFeeder = false;
	SendableChooser<CommandBase> autoChooser;
	double hoodHoldPosition = 0;
	boolean holdHood = false;
	public boolean setRPMThroughDash = true;
	private Debouncer intakeDebouncer = new Debouncer(1),
			autoFeedDebouncer = new Debouncer(4),
			reverseDebouncer = new Debouncer(.125),
			feedAfterReverseDebouncer = new Debouncer(4);

	private Controller driverController, operatorController;
	private Drivetrain drivetrain;
	private Feeder feeder;
	private Intake intake;
	private Shooter shooter;
	private Shooter.Turret turret;
	private Shooter.Hood hood;
	public Climber climber;
	public TalonFX climberMotor;
	private Accelerator accelerator;
	private SmartNumber dashVelGoal, dashHoodSetpoint;
	private SmartBoolean dashAcceleratorDetect, dashFeederDetect, dashManualHood;
	private SmartBoolean zeroGyro, zeroTurret, zeroHood;
	private DigitalInput acceleratorBallSensor, feederBallSensor;
	private Pose2d smartSetOffset = new Pose2d(-10, 16.5, new Rotation2d());

	private Superstructure() {
		autoChooser = new SendableChooser<>();

		OI oi = OI.getInstance();
		driverController = oi.getDriverController();
		operatorController = oi.getOperatorController();

//		oi.setXBoxDeadbands(true);
		oi.setDualshockDeadbands(true);

//        oi.setXBoxDeadbands(false);
		oi.setDualshockDeadbands(false);

		drivetrain = Drivetrain.getInstance();
		feeder = Feeder.getInstance();
		intake = Intake.getInstance();
		shooter = Shooter.getInstance();
		hood = Shooter.getHood();
		climber = Climber.getInstance();
		turret = Shooter.getTurret();
		accelerator = Accelerator.getInstance();
		dashVelGoal = new SmartNumber("Set shooter goal RPM", Shooter.CLOSE_RPM);
		zeroGyro = new SmartBoolean("Zero Gyro", false);
		zeroTurret = new SmartBoolean("Zero Turret", false);
		zeroHood = new SmartBoolean("Zero Hood", false);
		climberMotor = new TalonFX(RobotMap.CLIMBER);
		climberMotor.setSelectedSensorPosition(FeedbackDevice.IntegratedSensor.value);
		acceleratorBallSensor = new DigitalInput(RobotMap.ACCELERATOR_SENSOR);
		feederBallSensor = new DigitalInput(RobotMap.FEEDER_SENSOR);
		dashAcceleratorDetect = new SmartBoolean("Accelerator Detected Ball", false);
		dashFeederDetect = new SmartBoolean("Feeder Detected Ball", false);
		new Compressor();

		dashHoodSetpoint = new SmartNumber("Set hood setpoint", 0);
		dashManualHood = new SmartBoolean("Control Hood Automatically", true);

		makeAutos();

		operatorController.setRightTriggerButtonThreshold(0.7);
		operatorController.setLeftTriggerButtonThreshold(0.7);
		operatorController.setRightStickXDeadband(.06);
		driverController.setRightStickYDeadband(0.15);
		driverController.setLeftStickYDeadband(0.14);
		oi.startPeriodicControllerUpdate();

		ledsOn();
		shooter.shootSetpoint = 4000;
		turret.setSmartSetOffset(-10, 16.5, 0);
		climberMotor.setNeutralMode(NeutralMode.Brake);
	}

	public void resetSmartSetPosition() {
		Pose2d pose = drivetrain.getCurrentPose();
		turret.setSmartSetOffset(pose.getTranslation().getX(), pose.getTranslation().getY(), pose.getRotation().getDegrees());
	}

	public static Superstructure getInstance() {
		return instance = instance == null ? new Superstructure() : instance;
	}

	public void initAuto() {
		Vision.getInstance(1).setDistanceRegion(Vision.DistanceRegion.Close);
		drivetrain.resetOdometry();
		drivetrain.setBrake();
		turret.setAngle(turret.getAngle());

		var cmd = autoChooser.getSelected();
		cmd.schedule();
	}

	boolean autoFeedToggle = true;
	boolean reverseAccelerator = false;

	public void runTeleOp() {
		SmartDashboard.putBoolean("Auto feed debouncer", autoFeedDebouncer.get());
		SmartDashboard.putNumber("Hood Angle", hood.getPosition());
		if (operatorController.aPressed()) {
			autoFeedToggle = !autoFeedToggle;
		}
		SmartDashboard.putBoolean("Auto feed toggle", autoFeedToggle);

		//DRIVE
		drivetrain.periodic();

		//Climber
		if (Math.abs(operatorController.getLeftStickY()) > .14) {
/*
			if (climberMotor.getSelectedSensorPosition() < -492000) {
				climberMotor.set(ControlMode.PercentOutput, operatorController.getLeftStickY() > 0 ? 0 : operatorController.getLeftStickY());
			} else if (climberMotor.getSelectedSensorPosition() > 0) {
				climberMotor.set(ControlMode.PercentOutput, operatorController.getLeftStickY() < 0 ? 0 : operatorController.getLeftStickY());
			} else {
*/
			climberMotor.set(ControlMode.PercentOutput, -operatorController.getLeftStickY());
			/*}*/
		} else {
			climberMotor.set(ControlMode.PercentOutput, 0);
		}
		// Intake
		if (!shooter.forceFeedCycle) {
			if (driverController.getRightBumper()) {        // Intake
				intake.run(1);
				intakeDebouncer.update(false);
				autoFeedDebouncer.update(false);
				reverseDebouncer.update(false);
				feedAfterReverseDebouncer.update(false);
				intake.setExtended(true);
				runAutoFeed();
//				feeder.run(-Feeder.BALL_FILL_POWER / 2);
			} else if (driverController.getLeftBumper()) {    // Reverse Intake
				intake.run(-1);
			} else if (driverController.b()) {            // Run Intake and Feed
				intake.run(1);
				runAutoFeed();
			} else {                                        // Idle Actions
				intakeDebouncer.update(true);
				reverseDebouncer.update(true);
				autoFeedDebouncer.update(true);

				intake.run(intakeDebouncer.get() ? 0 : 1);
				if (shooter.getState() == Shooter.Goal.IDLE) {
/*
					intake.run(0);
					if (!reverseDebouncer.get()) {
						accelerator.run(0);
						feeder.run(-Feeder.BALL_FILL_POWER / 2);
					} else {
						feedAfterReverseDebouncer.update(true);
						if (autoFeedToggle && !feedAfterReverseDebouncer.get()) {
							runAutoFeed();
						} else {
							feeder.run(0);
							accelerator.run(0);
						}
					}
*/
					intake.run(0);
					if (autoFeedToggle) {
						runAutoFeed();
					} else {
						feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER : 0);
						accelerator.run(reverseAccelerator ? -Accelerator.FEED_SHOOTER_POWER : 0);
					}
				} else if (!shooter.optimalShootingConditions) {
					feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER : 0);
					accelerator.run(reverseAccelerator ? -Accelerator.FEED_SHOOTER_POWER : 0);
				}
				intake.setExtended(false);
			}
		}

		//Feeder
		reverseFeeder = driverController.x();
		reverseAccelerator = operatorController.getDPadDown();

		if (!driverController.y()) {
			shooter.keepShooting = false;
			shooter.wantsToShoot = false;
			shooter.forceFeedCycle = false;
		}
		//SHOOTER
		if (operatorController.getLeftStickPressPressed()) {                    // Rev on idle
			shooter.toggleRevOnIdle();
		}
		if (driverController.yPressed()) {
//			hoodHoldPosition = hood.getCANCoder().getAbsolutePosition();        // Shoot
			hoodHoldPosition = hood.getPosition();        // Shoot
		}

		if (driverController.y()) {                                            // Shoot
			holdHood = true;
			ledsOn();
			shooter.setState(Shooter.Goal.SHOOT);
		} else if (operatorController.getLeftBumper()) {                        // Aim
			holdHood = false;
			ledsOn();
			if (Vision.getInstance().getLimelight1().targetFound()) {
				shooter.setState(Shooter.Goal.LOCKED_ON);
			} else if (operatorController.getLeftStickPress()) {
				turret.smartSet();
			} else {
				controlTurretManual();
			}
		} else if (operatorController.getRightBumper()) {                        // Rev Shooter
			holdHood = false;
			ledsOff();
			shooter.setState(Shooter.Goal.REV);
			if (operatorController.getLeftStickPress()) {
				turret.smartSet();
			} else {
				controlTurretManual();
			}
		} else {                                                                // Idle
			holdHood = false;
			ledsOff();
			shooter.setState(Shooter.Goal.IDLE);
			if (operatorController.getLeftStickPress()) {
				turret.smartSet();
			} else {
				controlTurretManual();
			}
		}

		if (operatorController.getBack()) {
			dashVelGoal.setValue(Shooter.FAR_RPM);
			shooter.shootSetpoint = Shooter.FAR_RPM;
			Vision.getInstance().setDistanceRegion(Vision.DistanceRegion.Far);
		} else if (operatorController.getStart()) {
			dashVelGoal.setValue(Shooter.CLOSE_RPM);
			shooter.shootSetpoint = Shooter.CLOSE_RPM;
			Vision.getInstance().setDistanceRegion(Vision.DistanceRegion.Close);
		}

		SmartDashboard.putNumber("Absolute Turret Angle", Shooter.getTurret().getAbsoluteAngle());

		shooter.updateStateMachine();
	}

	void controlTurretManual() {
		turret.incrementAngle(-operatorController.getRightStickX() * TURRET_MANUAL_SPIN_SCALE);

/*
        if (operatorController.x()) {
            turret.incrementAngle(20);
        } else if (operatorController.b()) {
            turret.incrementAngle(-20);
        }
*/
	}

	@Override
	public void periodic() {
		SmartDashboard.putNumber("Hood Position", hood.getPosition());
		if (zeroGyro.getValue()) {

			drivetrain.resetOdometry();
			zeroGyro.setValue(false);
		}

		if (zeroTurret.getValue()) {
			turret.zero();
			zeroTurret.setValue(false);
		}

		if (zeroHood.getValue()) {
			hood.zero();
			zeroHood.setValue(false);
		}

		if (dashManualHood.getValue()) {
			if (holdHood) {
				hood.magSet(hoodHoldPosition);
			} else if (Vision.getInstance().getLimelight1().targetFound()) {
				hood.setPositionWithDistance();
			} else {
				hood.stop();
			}
		} else {
			hood.magSet(dashHoodSetpoint.getValue());
		}

		intakeDebouncer.update(true);

		dashAcceleratorDetect.setValue(!acceleratorBallSensor.get());
		dashFeederDetect.setValue(!feederBallSensor.get());
	}

	Timer timer = new Timer();
	boolean timerStarted = false;

	void runAutoFeed() {
		if (!shooter.wantsToShoot) {
			boolean feederHasBall = !feederBallSensor.get(),
					acceleratorHasBall = !acceleratorBallSensor.get();

			if (!acceleratorHasBall) {
				accelerator.run(reverseAccelerator ? -Accelerator.FEED_SHOOTER_POWER : Accelerator.FILL_ACCELERATOR_POWER);
				feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER / 2 : Feeder.BALL_FILL_POWER);
			} else if (feederHasBall) {
				feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER / 2 : -Feeder.BALL_FILL_POWER);
				accelerator.run(reverseAccelerator ? -Accelerator.FEED_SHOOTER_POWER : 0);
				timer.reset();
			} else {
//				feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER / 2 : 0);
				accelerator.run(reverseAccelerator ? -Accelerator.FEED_SHOOTER_POWER : 0);

				if (!timerStarted) {
					timer.start();
					timerStarted = true;
				}
				feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER / 2 : Feeder.BALL_FILL_POWER);
//				if (timer.get() < 0.350) {
//					feeder.run(reverseFeeder ? -Feeder.FEED_SHOOTER_POWER / 2 : -Feeder.BALL_FILL_POWER);
//				} else if(timer.get() < .5 && false) {
//
//				} else {
//					timer.reset();
//				}
			}
		}
	}

	public void ledsOn() {
		Vision.getInstance(1).getLimelight1().setLEDMode(Limelight.LEDMode.On);
	}

	public void ledsOff() {
		Vision.getInstance(1).getLimelight1().setLEDMode(Limelight.LEDMode.Off);
	}

	void makeAutos() {
		final double yScale = 1;

		try {
			final double trenchX = -7.0;
			final double trenchY = 0.0;


			final double shot1X = trenchX + 5.5;
			final double shot1Y = trenchY - 13.5;

			final double pickUp2X = shot1X - 5;
			final double pickUp2Y = shot1Y;

			final double backupX = pickUp2X + 0.93 / 2;
			final double backupY = pickUp2Y + 0.34 / 2;

			final double thirdBallX = backupX - 2.5;
			final double thirdBallY = backupY + 0.0;

			final double shot2X = thirdBallX + 2;
			final double shot2Y = thirdBallY - 2;

			final double thirdPathX = shot1X - 6;
			final double thirdPathY = shot1Y;

			final double finalShotXInt = shot1X - 4.25;
			final double finalShotYInt = shot1Y + .25;


			final double finalShotX = shot1X - 7.5;
			final double finalShotY = shot1Y + 5;

			DifferentialDriveVoltageConstraint constraint = new DifferentialDriveVoltageConstraint(drivetrain.getFeedforward(), drivetrain.getKinematics(), 8.0);
			TrajectoryConfig towardsInitiation = new TrajectoryConfig(drivetrain.kMaxLinearSpeedFeetPerSecond, drivetrain.kMaxLinearSpeedFeetPerSecond / 2.0)
					.addConstraint(constraint);

			TrajectoryConfig awayFromInitiationFull = new TrajectoryConfig(drivetrain.kMaxLinearSpeedFeetPerSecond, drivetrain.kMaxLinearSpeedFeetPerSecond / 2.0)
					.addConstraint(constraint)
					.setReversed(true);

			TrajectoryConfig awayFromInitiationSlow = new TrajectoryConfig(6 * drivetrain.kMaxLinearSpeedFeetPerSecond / 14.2, drivetrain.kMaxLinearSpeedFeetPerSecond / 2.0)
					.addConstraint(constraint)
					.setReversed(true);

			TrajectoryConfig config4 = new TrajectoryConfig(drivetrain.kMaxLinearSpeedFeetPerSecond / 2.0, drivetrain.kMaxLinearSpeedFeetPerSecond / 2.0)
					.addConstraint(constraint);

			TrajectoryConfig goToBallGridConfig = new TrajectoryConfig(drivetrain.kMaxLinearSpeedFeetPerSecond / 4, drivetrain.kMaxLinearSpeedFeetPerSecond / 4.0)
					.addConstraint(constraint)
					.setReversed(true);

			// new Translation2d(3,3)
			Trajectory initiationToTrench = TrajectoryGenerator.generateTrajectory(
					new Pose2d(0, 0, Rotation2d.fromDegrees(0)),
					List.of(
							// new Translation2d(3,3)
					),
					new Pose2d(trenchX, trenchY, Rotation2d.fromDegrees(0)),
					awayFromInitiationFull);

			Trajectory trenchToShooting = TrajectoryGenerator.generateTrajectory(
					new Pose2d(trenchX, trenchY, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(shot1X, shot1Y, Rotation2d.fromDegrees(-90)),
					towardsInitiation);

			Trajectory shootingTo2Balls = TrajectoryGenerator.generateTrajectory(
					new Pose2d(shot1X, shot1Y, Rotation2d.fromDegrees(-90.0)),
					List.of(),
					new Pose2d(pickUp2X, pickUp2Y, Rotation2d.fromDegrees(22.5)), awayFromInitiationSlow);

			Trajectory grabThirdBall = TrajectoryGenerator.generateTrajectory(
					new Pose2d(pickUp2X, pickUp2Y, Rotation2d.fromDegrees(22.5)),
					List.of(),
					new Pose2d(backupX, backupY, Rotation2d.fromDegrees(22.5)),
					towardsInitiation);
			Trajectory thirdBallLocation = TrajectoryGenerator.generateTrajectory(
					new Pose2d(backupX, backupY, Rotation2d.fromDegrees(22.5)),
					List.of(),
					new Pose2d(thirdBallX, thirdBallY, Rotation2d.fromDegrees(0)),
					awayFromInitiationFull);
			Trajectory ballsToFinalShot = TrajectoryGenerator.generateTrajectory(
					new Pose2d(thirdBallX, thirdBallY, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(shot2X, shot2Y, Rotation2d.fromDegrees(-90.0)), towardsInitiation);


			Trajectory trenchToShotClose = TrajectoryGenerator.generateTrajectory(
					new Pose2d(-8.0, 0, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(6.5, -14.5, Rotation2d.fromDegrees(-50)),
					towardsInitiation);
			Trajectory shotCloseTo3Balls = TrajectoryGenerator.generateTrajectory(
					new Pose2d(6.5, -14.5, Rotation2d.fromDegrees(-50)),
					List.of(),
					new Pose2d(-9, -12, Rotation2d.fromDegrees(10)),
					awayFromInitiationFull);
			Trajectory initiationToDeepTrench = TrajectoryGenerator.generateTrajectory(
					new Pose2d(0, 0, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(-13.5, 0, Rotation2d.fromDegrees(0)),
					awayFromInitiationFull);
			Trajectory initiationToJTurn = TrajectoryGenerator.generateTrajectory(
					new Pose2d(0, 0, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(-9.0, 8.5, Rotation2d.fromDegrees(30)),
					awayFromInitiationFull);
			Trajectory jTurnToInitiation = TrajectoryGenerator.generateTrajectory(
					new Pose2d(-9.0, 8.5, Rotation2d.fromDegrees(30)),
					List.of(),
					new Pose2d(0, 0, Rotation2d.fromDegrees(0)),
					towardsInitiation);
			Trajectory deepTrenchToShot = TrajectoryGenerator.generateTrajectory(
					new Pose2d(-13.5, 0, Rotation2d.fromDegrees(0)),
					List.of(),
					new Pose2d(0, 3, Rotation2d.fromDegrees(30)),
					towardsInitiation);

			ArrayList<Translation2d> ps = new ArrayList<>();
			ps.add(new Translation2d(finalShotXInt, finalShotYInt));
			Trajectory goToBallGrid = TrajectoryGenerator.generateTrajectory(
					new Pose2d(shot1X, shot1Y, Rotation2d.fromDegrees(0)),
					ps,
					new Pose2d(finalShotX, finalShotY, Rotation2d.fromDegrees(-90)),
					goToBallGridConfig);

			// for(var state : trenchToShooting.getStates()){
			// 	System.out.println(state.toString());

			// }
//        var farCourt = driveIntakeRev(drivetrain, initiationToTrench)
//                .andThen(driveRevThenShoot(drivetrain, trenchToShooting, 3.5))
//                .andThen(driveIntakeRev(drivetrain, shootingTo2Balls))
//                //.andThen(new WaitCommand(1))
//                .andThen(new DrivePath(drivetrain, grabThirdBall))
//                //.andThen(new WaitCommand(1))
//                .andThen(driveIntakeRev(drivetrain, thirdBallLocation))
//                .andThen(new IntakeCmd(false))
//                //.andThen(new WaitCommand(1))
//                .andThen(driveRevThenShoot(drivetrain, ballsToFinalShot, 3.0));

			var goToGridAndShoot = new ParallelCommandGroup(
					driveIntakeRev(drivetrain, goToBallGrid),
					new SetTurretAngleCmd(-20)
			);

			var farCourt = new SequentialCommandGroup(
					driveIntakeRev(drivetrain, initiationToTrench),
					driveRevThenShoot(drivetrain, trenchToShooting, 3.5)
//					new TurnInPlaceCmd(drivetrain, 0).withTimeout(2)
//					goToGridAndShoot

//				driveRevThenShoot(drivetrain, shootingTo2Balls, 5.5)
//				new DrivePath(drivetrain, grabThirdBall),
//				driveIntakeRev(drivetrain, thirdBallLocation),
//				driveRevThenShoot(drivetrain, ballsToFinalShot, 3.0)
			);

			var closeCourt = driveAndIntake(drivetrain, initiationToTrench)
					.andThen(driveRevThenShoot(drivetrain, trenchToShotClose, 3.5));
//				.andThen(driveAndIntake(drivetrain, shotCloseTo3Balls))
//				.andThen(driveRevThenShoot(drivetrain, ballsToFinalShot, 3.0));
			/**add the rest of the path tomorrow:
			 * after shot close, pick up 2/3 balls from the usual bump
			 * possibly drive back to shoot again
			 * */
			var rightTrenchFar = driveIntakeRev(drivetrain, initiationToJTurn)
					.andThen(new ShootCmd().withTimeout(3.5))
					.andThen(new DrivePath(drivetrain, jTurnToInitiation))
					.andThen(driveAndIntake(drivetrain, initiationToDeepTrench))
					.andThen(driveRevThenShoot(drivetrain, deepTrenchToShot, 3.0));


			autoChooser.addOption("Far Court", farCourt);
			autoChooser.addOption("Close Court", closeCourt);
			autoChooser.addOption("JTurn Deep Trench", rightTrenchFar);
			SendableRegistry.setName(autoChooser, "Auto");
			SmartDashboard.putData("Auto Chooser", autoChooser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CommandBase driveIntakeRev(Drivetrain drivetrain, Trajectory randomTrajectory) {
		var doEverythingButShoot = new IntakeCmd(true).alongWith(new DrivePath(drivetrain, randomTrajectory)).alongWith(new RevShooterCmd());
		return doEverythingButShoot;
	}

	public CommandBase driveRevThenShoot(Drivetrain drivetrain, Trajectory randomTrajectory, double shotSeconds) {
		var driveRevThenShoot = (new DrivePath(drivetrain, randomTrajectory).alongWith(new RevShooterCmd()))
				.andThen(new ShootCmd().withTimeout(shotSeconds)).alongWith(new IntakeCmd(false));
		return driveRevThenShoot;
	}

	public CommandBase driveAndIntake(Drivetrain drivetrain, Trajectory randomTrajectory) {
		var driveAndIntake = new DrivePath(drivetrain, randomTrajectory).alongWith(new IntakeCmd(true));
		return driveAndIntake;
	}


	/* ToDo: Controls
	 * Driver:
	 *   Right Trigger: Forwards
	 *   Left Trigger: Backwards
	 *   Left Stick X: Turn
	 *   Right Bumper: Intake Balls
	 *   Left Bumper: Run Intake Backwards
	 *   B: Run Intake Forwards
	 *   X: Reverse Feeder
	 *   Y: Shoot
	 *
	 *
	 * Operator:
	 * 	 A: Auto Feed Toggle (Starts on)
	 *   Left Bumper: Aim
	 *   Right Bumper: Rev Shooter
	 *   Left Stick: Toggle rev on idle
	 *   Left Stick while revving shooter: Smart set
	 *   Left Stick Y: Climber control
	 * 	 Start: Close shot
	 *   Back: Far Shot
	 *   (Climber translation outputs should increase the longer a translate button is held)
	 *   (Feeding/Hopping system should run autonomously based on ball indexes)
	 * */
	
	 /*
		L3 = Intake Toggle
		B = Run feeder forward
		X = Run feeder backward
		Y = Run accelerator up
		A = Run accelerator down
	 */
}
