package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.OI;
import frc.robot.RobotMap;
import frc.robot.logging.Loggable;
import frc.robot.utils.EfficentFalcon;

public class Climber extends SubsystemBase implements Loggable {
    public static final double kTargetPos = 2048f * 3.0;
    static final int kSlotIdx = 0;
    static final int kPIDLoopIdx = 0;
    static final int kTimeoutMs = 30;
    static final double CLIMB_CURRENT = 0;// NEED TO CHANGE
    static final double CONVERSION_TICKS_TO_FEET = (0.75 * Math.PI) / (12 * 18.17 * 2048);
    static final double CONVERSION_FEET_TO_TICKS = 1 / CONVERSION_TICKS_TO_FEET;
    static final double LOWEST_GRAB_POS = (CONVERSION_FEET_TO_TICKS * (1.6875 + 1)),
            HIGHEST_GRAB_POS = (CONVERSION_FEET_TO_TICKS * (4.01 + 0.84)),
            LOWEST_CLIMB_POS = (CONVERSION_FEET_TO_TICKS * (1.5 + 0.84));
    static final double CLIMB_SPEED = -1,
            GRAB_SPEED = -0.8,
            POST_SPEED = -0.1;
    static final boolean CLIMB_BRAKED = true,
            CLIMB_RELEASED = !CLIMB_BRAKED;
    static final double WRANGLER_SPEED = 1; // change if needed including - or +
    private static Climber instance;
    public final double kP;
    public final double kI;
    public final double kD;
    public final double kF;
    public final int kIzone;
    public final double kPeakOutput;
    final int maxVelocity;
    final int maxAcceleration;
    EfficentFalcon m_climberMotor;
    TalonSRX m_wranglerMotor;
    double m_wranglerSpeed;
    Solenoid m_climbBrake;
    boolean m_climbBrakeState;
    double m_climberSpeed;
    ClimbStates m_climbState;
    Timer m_climbTimer;
    boolean m_bResetTimer;
    TalonFXSensorCollection m_climberSensors;
    TalonFXControlMode m_controlMode;
    double m_climbPosition;

    private Climber() {
        m_climbTimer = new Timer();
        m_climberMotor = new EfficentFalcon(RobotMap.CLIMBER);
        m_climberMotor.setNeutralMode(NeutralMode.Brake);
        m_wranglerMotor = new TalonSRX(RobotMap.WRANGLER);
        m_wranglerSpeed = 0;
        m_climbBrake = new Solenoid(RobotMap.CLIMBER_BRAKE);
        m_climbBrakeState = CLIMB_RELEASED;
        m_controlMode = TalonFXControlMode.PercentOutput;
        m_climbState = ClimbStates.DEFAULT;
        m_wranglerMotor.setNeutralMode(NeutralMode.Brake);
        m_wranglerMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 0.5));
        m_wranglerMotor.configContinuousCurrentLimit(70);
        m_wranglerMotor.enableCurrentLimit(true);
        m_climberSpeed = 0;
        m_bResetTimer = true;
        m_climberSensors = m_climberMotor.getSensorCollection();
        m_climbPosition = kTargetPos;
        kP = 0.46376;
        kI = 0;
        kD = 0;
        kF = 0.04335;
        kIzone = 0;
        kPeakOutput = 1;
        maxAcceleration = 1000;
        maxVelocity = 4000;
        m_climberMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
        m_climberMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));

    }

    public static Climber getInstance() {
        return instance = instance == null ? new Climber() : instance;
    }

    public void runTeleOp() {
        switch (m_climbState) {
            case DEFAULT:
                defaultState();
                break;
            case MANUALUP:
                manualUp();
                break;
            case MANUALDOWN:
                manualDown();
                break;
            case PRECLIMB:
                preClimbState();
                break;
            case GRAB:
                grabState();
                break;
            case RETRACTED:
                retractedClimb();
                break;
            case POSTCLIMB:
                postClimb();
                break;
        }
        if (m_controlMode == TalonFXControlMode.MotionMagic) {
            m_climberMotor.set(m_controlMode, m_climbPosition);
        } else {
            m_climberMotor.set(m_controlMode, m_climberSpeed);
        }
        m_climbBrake.set(m_climbBrakeState);
        printSmartDashboardInfo();
    }

    private void defaultState() {
        m_climbBrakeState = CLIMB_RELEASED;
        m_climberSpeed = 0;
        m_controlMode = TalonFXControlMode.PercentOutput;
    }

    private void manualUp() {
        m_climbBrakeState = CLIMB_RELEASED;
        m_climberSpeed = 0.9;
        m_controlMode = TalonFXControlMode.PercentOutput;
    }

    private void manualDown() {
        m_climbBrakeState = CLIMB_RELEASED;
        m_climberSpeed = -0.9;
        m_controlMode = TalonFXControlMode.PercentOutput;
    }

    private void preClimbState() {
        m_climbBrakeState = CLIMB_RELEASED;
        m_climbPosition = HIGHEST_GRAB_POS;
        m_controlMode = TalonFXControlMode.MotionMagic;
    }

    private void grabState() {
        m_climberSpeed = GRAB_SPEED;
        m_controlMode = TalonFXControlMode.PercentOutput;
        if (m_climberMotor.getSupplyCurrent() >= CLIMB_CURRENT) {
            m_climbState = ClimbStates.RETRACTED;
        } else if (m_climberSensors.getIntegratedSensorPosition() <= LOWEST_GRAB_POS) {
            m_climbState = ClimbStates.DEFAULT;
        }
    }

    private void retractedClimb() {
        m_climberSpeed = CLIMB_SPEED;
        m_controlMode = TalonFXControlMode.PercentOutput;
        if (m_climberSensors.getIntegratedSensorPosition() < LOWEST_CLIMB_POS) {
            m_climbState = ClimbStates.POSTCLIMB;
        }
    }

    private void postClimb() {
        m_climberSpeed = POST_SPEED;
        m_controlMode = TalonFXControlMode.PercentOutput;
        m_climbBrakeState = CLIMB_BRAKED;
    }

    public ClimbStates getClimbState() {
        return m_climbState;
    }

    public void setClimbState(ClimbStates state) {
        m_climbState = state;
    }

    private void motionMagic() {
        m_climbState = ClimbStates.DEFAULT;
        m_climberMotor.setSelectedSensorPosition(0);

        m_climberMotor.configFactoryDefault();

        m_climberMotor.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, kPIDLoopIdx,
                kTimeoutMs);

        m_climberMotor.configNeutralDeadband(0.001, kTimeoutMs);

        m_climberMotor.setSensorPhase(false);
        m_climberMotor.setInverted(false);

        m_climberMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, kTimeoutMs);
        m_climberMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, kTimeoutMs);

        m_climberMotor.configNominalOutputForward(0, kTimeoutMs);
        m_climberMotor.configNominalOutputReverse(0, kTimeoutMs);
        m_climberMotor.configPeakOutputForward(1, kTimeoutMs);
        m_climberMotor.configPeakOutputReverse(-1, kTimeoutMs);

        m_climberMotor.selectProfileSlot(kSlotIdx, kPIDLoopIdx);
        m_climberMotor.config_kF(kSlotIdx, kF, kTimeoutMs);
        m_climberMotor.config_kP(kSlotIdx, kP, kTimeoutMs);
        m_climberMotor.config_kI(kSlotIdx, kI, kTimeoutMs);
        m_climberMotor.config_kD(kSlotIdx, kD, kTimeoutMs);

        m_climberMotor.configMotionCruiseVelocity(maxVelocity, kTimeoutMs);
        m_climberMotor.configMotionAcceleration(maxAcceleration, kTimeoutMs);

        m_climberMotor.setSelectedSensorPosition(0, kPIDLoopIdx, kTimeoutMs);
    }
    private String climberControlMode() {
        switch (m_controlMode) {
            case MotionMagic:
                return "Motion Magic";
            case PercentOutput:
                return "Percent Output";
        }
        return "Unexpected Control Mode";
    }

    public void robotInit() {
        motionMagic();
        SmartDashboard.putNumber("Climb Output", 0.0);
        SmartDashboard.putNumber("Climb Position In Feet", 0.0);
        SmartDashboard.putNumber("Climb Current", 0.0);
        SmartDashboard.putBoolean("Break Position", false);
        SmartDashboard.putString("Current State", "");
        SmartDashboard.putString("Control Mode", "");
        SmartDashboard.putNumber("Encoder Value", 0.0);
        SmartDashboard.putNumber("Wrangler Speed", 0.0);
    }

    private void wranglerControl() {
        if (OI.getInstance().getOperatorController().getDPadLeft()) {
            m_wranglerMotor.set(ControlMode.PercentOutput, WRANGLER_SPEED);
        } else if (OI.getInstance().getOperatorController().getDPadRight()) {
            m_wranglerMotor.set(ControlMode.PercentOutput, -WRANGLER_SPEED);
        } else {
            m_wranglerMotor.set(ControlMode.PercentOutput, 0);
        }
    }

    public void testControl(double motorSpeed, boolean breakPosition) {
        m_climberMotor.set(TalonFXControlMode.PercentOutput, motorSpeed);
        m_climbBrake.set(breakPosition);
        wranglerControl();
        printSmartDashboardInfo();
    }

    public void printSmartDashboardInfo() {
        SmartDashboard.putNumber("Climb Output", m_climberMotor.getMotorOutputPercent());
        SmartDashboard.putNumber("Climb Position In Feet", m_climberSensors.getIntegratedSensorPosition() * CONVERSION_TICKS_TO_FEET);
        SmartDashboard.putNumber("Climb Current", m_climberMotor.getSupplyCurrent());
        SmartDashboard.putBoolean("Break Position", m_climbBrake.get());
        SmartDashboard.putString("Current State", m_climbState.str);
        SmartDashboard.putString("Control Mode", climberControlMode());
        SmartDashboard.putNumber("Encoder Value", m_climberSensors.getIntegratedSensorPosition());
        SmartDashboard.putNumber("Wrangler Speed", m_wranglerSpeed);
        SmartDashboard.putNumber("Velocity", m_climberSensors.getIntegratedSensorVelocity());
    }

    public double getEncoderTicks() {
        return m_climberMotor.getSelectedSensorPosition();
    }

    @Override
    public String[] setCSVHeader() {
        return new String[]{"CLimb Speed", "Climb Position", "Climb Current", "Break Position"};
    }

    @Override
    public String[] toCSVData() {
        return new String[]{m_climberMotor.getMotorOutputPercent() + "",
                m_climberSensors.getIntegratedSensorPosition() + "", m_climberMotor.getSupplyCurrent() + "", m_climbBrake.get() + ""};
    }

    public enum ClimbStates {
        DEFAULT("Default"),
        MANUALUP("Manual Up"),
        MANUALDOWN("Manual Down"),
        PRECLIMB("Pre Climb"),
        GRAB("Grab"),
        RETRACTED("Retracted"),
        POSTCLIMB("Post Climb");

        final String str;

        private ClimbStates(String str) {
            this.str = str;
        }

        String getString() {
            return str;
        }
    }
}