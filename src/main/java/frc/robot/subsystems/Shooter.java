package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.sensors.CANCoder;
import com.revrobotics.*;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.logging.Loggable;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import static frc.robot.subsystems.Shooter.ShooterConstants.*;

/**
 * Shooter class for the piece of scrap metal we are making
 */
public class Shooter extends SubsystemBase implements Loggable {
	public static final String LOCKED_ON = "LOCKED ON";
	public static final double CLOSE_RPM = 4000, FAR_RPM = 4700; // far rpm was 5500 (7/25/2021)
	public static final double ACCEPTABLE_ERROR = 150;
	private static final double DEFAULT_I = 0.000000; //ToDo: Delete from codebase
	private static Shooter instance;
	private static Turret turretInstance;
	private static Hood hoodInstance;
	private final Debouncer acceleratorDebouncer = new Debouncer(0.2);
	public EfficientFalcon shooter, shooterFollower;
	public double localP = 0, localI = 0, localD = 0, localFF = 0, localIZone = 0;
	public double shooterReference = 0;
	public double localShooterRPM = 0;
	public double shootSetpoint = 4200;
	double dt = 0;
	double previousTime = 0;
	double oldShooterSetpoint = 0;
	boolean keepShooting = false;
	boolean forceFeedCycle = false;
	boolean wantsToShoot = false;
	public boolean overrideHood = false;
	private MovingAverage shooterSetpointMA = new MovingAverage(2.5);
	public Goal currentGoal = Goal.IDLE;
	private String status = "";
	private Vision vision;
	private Limelight limelight;
	private double trackedDistance = 0;
	private double idleVelocity = 0;
	private boolean idleVelocityToggle = true;

	private Shooter() {
		shooter = new EfficientFalcon(RobotMap.LEFT_FLYWHEEL);
		shooterFollower = new EfficientFalcon(RobotMap.BACKSPINNER);

		shooterFollower.follow(shooter, FollowerType.PercentOutput);

		shooter.setInverted(TalonFXInvertType.Clockwise);
		shooterFollower.setInverted(TalonFXInvertType.FollowMaster);
		shooter.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 0);
		shooterFollower.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 0);

		shooterSetpointMA.preFill();

		shooter.config_kP(0, P);
		shooter.config_kI(0, I);
		shooter.config_kD(0, D);
		shooter.config_kF(0, FF);
		shooter.config_IntegralZone(0, (int) IZONE);

		vision = Vision.getInstance(1);
		limelight = vision.getLimelight1();

		shooter.configVoltageCompSaturation(12);
		shooter.enableVoltageCompensation(true);
		shooter.setNeutralMode(NeutralMode.Coast);

		shooter.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 100, 3, 1));
		shooterFollower.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 100, 3, 1));

		shooter.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		shooterFollower.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
	}

	public static Shooter getInstance() {
		return instance = instance == null ? new Shooter() : instance;
	}

	///////////////////////////////////////////////////////////////////////////
	// State Machine
	///////////////////////////////////////////////////////////////////////////

	public static Turret getTurret() {
		return turretInstance = turretInstance == null ? new Turret() : turretInstance;
	}

	public static Hood getHood() {
		return hoodInstance = hoodInstance == null ? new Hood() : hoodInstance;
	}

	public void setState(Goal goal) {
		currentGoal = goal;
	}

	public Goal getState() {
		return currentGoal;
	}

	public void toggleRevOnIdle() {
		idleVelocityToggle = !idleVelocityToggle;
		if (idleVelocityToggle) {
			idleVelocity = 1500;
		} else {
			idleVelocity = 0;
		}
	}

	public boolean optimalShootingConditions = false;

	public void updateStateMachine() {
		optimalShootingConditions = limelight.targetFound() && Math.abs(getShooterClosedLoopError(shootSetpoint)) < ACCEPTABLE_ERROR;
		SmartDashboard.putBoolean("Optimal shooting conditions", optimalShootingConditions);
		SmartDashboard.putString("Shooter state goal", currentGoal.name());
		SmartDashboard.putNumber("Shooter Applied Output", shooter.getMotorOutputPercent());
		getTurret().updateSmartSet();
		SmartDashboard.putNumber("Smart Turret Angle", getTurret().smartSetAngle);

		if (Superstructure.getInstance().setRPMThroughDash) {
			this.localShooterRPM = getShooterRPM();
			shootSetpoint = SmartDashboard.getNumber("Set shooter goal RPM", oldShooterSetpoint);
			oldShooterSetpoint = shootSetpoint;
		}

		if (limelight.targetFound()) {
			trackedDistance = vision.getDistance();
		}

		switch (currentGoal) {
			case IDLE:
				wantsToShoot = false;
				keepShooting = false;
				forceFeedCycle = false;
				if (Math.abs(localShooterRPM) <= 10) {
					this.status = "Idle";
				} else {
					this.status = "Idling...";
				}

				if (!idleVelocityToggle) {
					setShooterVel(idleVelocity);
				} else {
					shooter.set(TalonFXControlMode.PercentOutput, 0);
				}
//                Accelerator.getInstance().run(0);
//                Feeder.getInstance().run(Superstructure.getInstance().reverseFeeder ? -1 : 0);
				break;
			case AIM_TURRET:
				forceFeedCycle = false;
				wantsToShoot = false;
				this.status = "Aiming Turret and Hood";
				getTurret().aim();
				if (!overrideHood) {
					getHood().setPositionWithDistance();
				}
				setShooterVel(idleVelocity);
//                Accelerator.getInstance().run(0);
//                Feeder.getInstance().run(Superstructure.getInstance().reverseFeeder ? -1 : 0);
				break;
			case REV:
				wantsToShoot = false;
				forceFeedCycle = false;
				this.status = "Revving shooter";
				setShooterVel(shootSetpoint);
//                Accelerator.getInstance().run(0);
//                Feeder.getInstance().run(Superstructure.getInstance().reverseFeeder ? -1 : 0);
				break;
			case LOCKED_ON: // If target not visible then set to previous captured vision distance
				keepShooting = false;
				forceFeedCycle = false;
				wantsToShoot = false;

				if (limelight.targetFound()) {
					getTurret().aim();
				}

				boolean lockedOn = limelight.targetFound() && Math.abs(getTurret().getSetpointError()) < .5;

				if (!limelight.targetFound() && Math.abs(getShooterClosedLoopError()) > ACCEPTABLE_ERROR) {
					this.status = "Revving Up, no target found";
				} else if (!limelight.targetFound() && Math.abs(getShooterClosedLoopError()) < ACCEPTABLE_ERROR) {
					this.status = "Revved Up, no target found";
				} else if (lockedOn) {
					this.status = LOCKED_ON;
				}

				setShooterVel(shootSetpoint);
//                Accelerator.getInstance().run(0);
//                Feeder.getInstance().run(Superstructure.getInstance().reverseFeeder ? -1 : 0);
				break;
			case SHOOT:
				optimalShootingConditions = limelight.targetFound() && Math.abs(getShooterClosedLoopError(shootSetpoint)) < ACCEPTABLE_ERROR;
				if (optimalShootingConditions || keepShooting) {
					this.status = "SHOOTING";
					keepShooting = true;
				} else {
					this.status = "Waiting for optimal shooting conditions";
				}

				setShooterVel(shootSetpoint);

				if (Vision.getInstance().getLimelight1().getXOffset() > 8) {
					getTurret().aim(); //Turret  error is too big then aim
					acceleratorDebouncer.update(false);
//                    Feeder.getInstance().run(0);
//                    Accelerator.getInstance().run(0);
				} else {
					if (optimalShootingConditions || keepShooting) {
						forceFeedCycle = true;
						wantsToShoot = true;
						acceleratorDebouncer.update(true);
						Accelerator.getInstance().run(Accelerator.FEED_SHOOTER_POWER);
						if (acceleratorDebouncer.get()) {
							Feeder.getInstance().run(Feeder.FEED_SHOOTER_POWER);
						}
						Intake.getInstance().run(1);
					} else {
						acceleratorDebouncer.update(false);
//                        Feeder.getInstance().run(0);
//                        Accelerator.getInstance().run(0);
					}
				}
				break;
		}

		putStatus();
		SmartDashboard.putNumber("Shooter Error", getShooterClosedLoopError(shootSetpoint));
		SmartDashboard.putNumber("Shooter RPM", getShooterRPM());
	}

	public String getStatus() {
		return this.status;
	}

	/**
	 * Sets the shooter to a power [-1, 1] with the motor controller's duty cycle
	 *
	 * @param power Power
	 */
	public void setShooterDutyCycle(double power) {
		shooter.set(ControlMode.PercentOutput, shooterReference = power);
	}

	///////////////////////////////////////////////////////////////////////////
	// Setters
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the shooter to a given velocity
	 *
	 * @param velocity Velocity
	 */
	public void setShooterVel(double velocity) {
		double ticksPer100ms = rpm2ticksPer100ms(shooterReference = velocity);
		shooterSetpointMA.update(ticksPer100ms);
		shooter.set(TalonFXControlMode.Velocity, shooterSetpointMA.get());
	}

	public void setShooterVelNoRamp(double velocity) {
		double ticksPer100ms = rpm2ticksPer100ms(shooterReference = velocity);
		shooter.set(TalonFXControlMode.Velocity, ticksPer100ms);
	}

	public void setShooterPercent(double power) {
		shooter.set(TalonFXControlMode.PercentOutput, power);
	}

	@Override
	public void periodic() {
		updateStateMachine();
	}

	public void putStatus() {
		SmartDashboard.putString("Shooter Status", status);
	}

	public double getShooterRPM() {
		return tp100ms2rpm(shooter.getSelectedSensorVelocity());
	}

	///////////////////////////////////////////////////////////////////////////
	// Getters
	///////////////////////////////////////////////////////////////////////////

	/*
	 * Shooter
	 */

	/**
	 * Gets main shooter motor's closed loop error
	 *
	 * @return Main shooter closed loop error
	 */
	public double getShooterClosedLoopError() {
		return tp100ms2rpm(shooter.getClosedLoopError());
	}

	public double getShooterClosedLoopError(double setpoint) {
		return setpoint - Math.abs(tp100ms2rpm(shooter.getSelectedSensorVelocity()));
	}

	/**
	 * Gets main shooter motor's applied output
	 *
	 * @return Main shooter applied output
	 */
	public double getMainShooterAppliedOutput() {
		return shooter.getMotorOutputPercent();
	}

	/**
	 * Gets main shooter motor's output current
	 *
	 * @return Main shooter output current
	 */
	public double getMainShooterOutputCurrent() {
		return shooter.getStatorCurrent();
	}

	/**
	 * Sets all motors to brake mode
	 */
	public void enableBrakeMode() {
		shooter.setNeutralMode(NeutralMode.Brake);
	}

	///////////////////////////////////////////////////////////////////////////
	// Modifiers
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Sets all motors to coast mode
	 */
	public void enableCoastMode() {
		shooter.setNeutralMode(NeutralMode.Coast);
	}

	public double tp100ms2rpm(double tp100ms) {
		double tpMs = tp100ms / 100; //Ticks per millisecond
		double tpm = tpMs * 60000; //Ticks per minute
		return tpm / INNER_FALCON_TICKS_PER_REVOLUTION;
	}

	public double rpm2ticksPer100ms(double rpm) {
		double tpm = rpm * INNER_FALCON_TICKS_PER_REVOLUTION;
		double tpms = tpm / 60000;
		return tpms * 100;
	}

	@Override
	public String[] setCSVHeader() {
		return new String[0];
	}

	@Override
	public String[] toCSVData() {
		return new String[0];
	}

	public enum Goal {IDLE, AIM_TURRET, REV, LOCKED_ON, SHOOT}

	/**
	 * HS-815BB
	 * Max travel in each direction: 270 degrees
	 * -1 to 1: least angle to greatest angle
	 */
	public static class Hood extends SubsystemBase implements Loggable {
		private final InterpolatingTreeMap<InterpolatingDouble, InterpolatingDouble> DISTANCE_TREE_MAP = new InterpolatingTreeMap<>();
		double previousError = 0;
		double previousTime = 0;
		private PWM servo1, servo2;
		private CANCoder encoder;
		private double hoodZeroOffset = (3108.78746 + 19) / 11.377;

		{
			try (var scan = new Scanner(new File(Filesystem.getDeployDirectory().getPath() + "/data/hood-angles-2.csv"))) { // FixMe: Use values that work with new Far rpm (hood-angles-2.csv)
				while (scan.hasNext()) {
					String[] strs = scan.nextLine().split(",");
					System.out.println("Putting into tree map: " + Arrays.toString(strs));
					DISTANCE_TREE_MAP.put(new InterpolatingDouble(Double.parseDouble(strs[0])), new InterpolatingDouble(Double.parseDouble(strs[1])));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				DISTANCE_TREE_MAP.put(new InterpolatingDouble(0.5), new InterpolatingDouble(0.5));
			}
		}

		private Hood() {
			servo1 = new PWM(0);
			servo2 = new PWM(1);
			encoder = new CANCoder(RobotMap.HOOD_ENCODER);
		}

		CANCoder getCANCoder() {
			return this.encoder;
		}

		public double getPosition() {
			return (encoder.getAbsolutePosition() - hoodZeroOffset) * 11.377;
		}

		public void zero() {
			hoodZeroOffset = encoder.getAbsolutePosition();
		}// * 11.377

		void setPositionWithDistance() {
			if (Vision.getInstance().getLimelight1().targetFound()) {
				setPositionWithDistance(Vision.getInstance(1).getDistance());
			}
			setPositionWithDistance(getInstance().trackedDistance);
		}

		void setPositionWithDistance(double distance) {
			var position = DISTANCE_TREE_MAP.getInterpolated(new InterpolatingDouble(distance));
			SmartDashboard.putNumber("Hood Setpoint", position.value);
			magSet(position.value);
		}

		public void odometrySet() {
			double x = Drivetrain.getInstance().getCurrentPose().getTranslation().getX();
			double y = Drivetrain.getInstance().getCurrentPose().getTranslation().getY();
			setPositionWithDistance(Math.hypot(x, y));
		}

		public void stop() {
			servo1.setPosition(0.5);
			servo2.setPosition(0.5);
		}

		public void magSet(double magTicks) { //magSet for now
			double currentTime = Timer.getFPGATimestamp();
			double dt = currentTime - previousTime;
			previousTime = currentTime;

			double error = -magTicks + getPosition();
			previousError = error;

			var command = (error * 0.05);
			var servo1Command = (command + 1) / 2;
			var servo2Command = (-command + 1) / 2; //((error * 0.0009) + 1) / 2;
			servo1.setPosition(servo1Command);
			servo2.setPosition(servo2Command);
		}

		@Override
		public String[] setCSVHeader() {
			return new String[0];
		}

		@Override
		public String[] toCSVData() {
			return new String[0];
		}
	}

	public static class Turret {
		private static final double GEAR_RATIO = 106.525742, MAX_ANGLE = 270, ALIGN_OFFSET = 1.33333333333333 / 2;
		CANSparkMax turretMotor;
		CANPIDController turretPIDController;
		CANEncoder turretEncoder;
		MovingAverage innerMA = new MovingAverage(.02);
		int slotID = 0;
		double smartSetAngle = 0;
		private ControlType controlType = ControlType.kPosition;
		private double setpoint, ssXOffset = 0, ssYOffset = 0, ssAngleOffset = 0;

		private Turret() {
			turretMotor = new CANSparkMax(RobotMap.TURRET, CANSparkMaxLowLevel.MotorType.kBrushless);
			turretPIDController = turretMotor.getPIDController();
			turretEncoder = turretMotor.getEncoder();

			turretEncoder.setPositionConversionFactor(360 / GEAR_RATIO);

			turretPIDController.setP(0.05);
			turretPIDController.setI(0.0005);
			turretPIDController.setIZone(3);
			turretPIDController.setD(0.05 * 20);
			turretPIDController.setIMaxAccum(0, 0);
			turretPIDController.setOutputRange(-0.4, 0.4);

			turretPIDController.setP(0.035, 1);
			turretPIDController.setI(0.000035, 1);
			turretPIDController.setIZone(1.5, 1);
			turretPIDController.setD(0.05 * 55, 1);
			turretPIDController.setOutputRange(-0.4, 0.4, 1);
			setpoint = getAngle();
		}

		public void zero() {
			turretEncoder.setPosition(setpoint = 0);
			turretPIDController.setReference(0, ControlType.kVelocity);
		}

		public void setSetpoint(double setpoint) {
			setpoint %= 360;
			if (setpoint > MAX_ANGLE) {
				this.setpoint = setpoint - 360;
			} else if (setpoint < -MAX_ANGLE) {
				this.setpoint = setpoint + 360;
			} else {
				this.setpoint = setpoint;
			}
		}

		public void setAngleToSetpoint() {
			turretPIDController.setReference(this.setpoint, ControlType.kVelocity, slotID);
		}

		public Turret incrementSetpoint(double angle) {
			setSetpoint(setpoint += angle);
			return this;
		}

		public void incrementAngle(double angle) {
			slotID = 0;
			setAngle(setpoint = getAngle() + angle);
		}

		public double getSetpointError() {
			return setpoint - getAngle();
		}

		public void updateSmartSet() {
			smartSetAngle = getSmartSetAngle();
		}

		public void aimInner() {
			slotID = 1;
			var tx = Vision.getInstance().getLimelight1().getXOffset();

			double innerPortTx = Vision.getInstance().getRobot2TargetPoseInRobotFrame().getRotation().getDegrees();

			var limelightAngleOffset = innerPortTx - tx;
			boolean isAngleWithinOuterPort = Math.abs(innerPortTx) < Vision.VisionConstants.INNER_PORT_OFFSET_LIMIT;
			SmartDashboard.putBoolean("Inner shot possible", isAngleWithinOuterPort);

			SmartDashboard.putNumber("Inner Port Tx", innerPortTx);
			SmartDashboard.putNumber("Limelight Angle Offset", limelightAngleOffset);
			double currentAngle = getAngle();
			if (Vision.getInstance().getLimelight1().targetFound() && isAngleWithinOuterPort) {
//				setAngle(currentAngle - tx);
				setAngle(currentAngle + innerMA.update(limelightAngleOffset).get()); //THIS LINE
				SmartDashboard.putNumber("Align Turret Reference", setpoint = currentAngle + innerMA.get());
			} else {
				setAngle(currentAngle);
				innerMA.update(0); //THIS LINE
				SmartDashboard.putNumber("Align Turret Reference", setpoint = currentAngle);
			}
		}

		public void aim() {
			slotID = 0;
			setAngle(getAngle() - Vision.getInstance(1).getLimelight1().getXOffset() - ALIGN_OFFSET);
		}

		public void setSmartSetOffset(double x, double y, double angle) {
			this.ssXOffset = x; // -10
			this.ssYOffset = y; // 19.135
			this.ssAngleOffset = angle;
		}

		double getSmartSetAngle() {
			Pose2d smartSetPose = new Pose2d(Drivetrain.getInstance().getCurrentPose().getTranslation().getX() + ssXOffset, Drivetrain.getInstance().getCurrentPose().getTranslation().getX() + ssYOffset, new Rotation2d());
			double ang = Math.atan2(smartSetPose.getTranslation().getY(), -smartSetPose.getTranslation().getX());
			ang = Math.toDegrees(ang);
			double turretAngle = Drivetrain.getInstance().getAngle() + ang + ssAngleOffset;

			// ALTERNATE CALC
			Pose2d goalPose = new Pose2d(ssXOffset, ssYOffset, Rotation2d.fromDegrees(ssAngleOffset));
			Pose2d relativePose = Drivetrain.getInstance().getCurrentPose().relativeTo(goalPose);
			double altAngle = relativePose.getRotation().getDegrees();

//            SmartDashboard.putString("Alternate Smart Pose", relativePose);
			SmartDashboard.putNumber("Alternate Turret Angle", altAngle);

			return -turretAngle;
		}

		public void smartSet() { //Rotate arctan by absolute turret angle
			setAngle(smartSetAngle);
		}

		public double getAngle() {
			return turretEncoder.getPosition();
		}

		public void setAngle(double angle) {
			angle %= 360;
			if (angle > MAX_ANGLE) {
				turretPIDController.setReference(setpoint = angle - 360, controlType, slotID);
			} else if (angle < -MAX_ANGLE) {
				turretPIDController.setReference(setpoint = angle + 360, controlType, slotID);
			} else {
				turretPIDController.setReference(setpoint = angle, controlType, slotID);
			}
			SmartDashboard.putNumber("Turret Setpoint", setpoint);
		}

		public double getAbsoluteAngle() {
			return (turretEncoder.getPosition() + Drivetrain.getInstance().getAngle()) % 360;
		}

		public void resetEncoder() {
			turretEncoder.setPosition(0);
			turretPIDController.setReference(0, ControlType.kDutyCycle);
		}
	}

	public static final class ShooterConstants {

		public static final double
				// Thank you for the PID constants Lemon Wires
				P = 0.6,       //Was 0.05
				I = 0.001,
				D = P * 20,
				IZONE = 300,
				FF = 0.04697602371/*0.056*/, //Was 0.062
				INNER_FALCON_TICKS_PER_REVOLUTION = 2048;
	}
}