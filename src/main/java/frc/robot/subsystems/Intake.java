package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.ControlType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.logging.Loggable;

/**
 * Class Representing Intake
 */
public class Intake extends SubsystemBase implements Loggable {
	private static Intake instance;
	CANSparkMax intakeSpinner;
	Solenoid intakeExtender;
	public static final double INTAKE_SPEED = -1, SPIT_SPEED = 0.5;
	
	private Intake() {
		intakeSpinner = new CANSparkMax(RobotMap.INTAKE_SPINNER, MotorType.kBrushless);
       intakeExtender = new Solenoid(RobotMap.INTAKE_EXTENDER);
		intakeSpinner.setInverted(false);
		intakeSpinner.setSmartCurrentLimit(30);
		intakeSpinner.burnFlash();
	}
	
	/**
	 * gets instance for intake
	 *
	 * @return instance of Intake
	 */
	public static Intake getInstance() {
		return instance = instance == null ? new Intake() : instance;
	}
	
	/**
	 * Sets motor speed and intake position
	 *
	 * @param speed    speed of the intake
	 * @param extended position of the intake
	 */
	private void set(double speed, boolean extended) {
		intakeSpinner.getPIDController().setReference(speed, ControlType.kDutyCycle);
       intakeExtender.set(extended);
	}
	
	public void setExtended(boolean extended) {
		intakeExtender.set(extended);
	}
	
	public void run(int forwardBackward) {
		intakeSpinner.set(forwardBackward);
	}
	
	public boolean extended() {
		return this.intakeExtender.get();
	}
	
	@Override
	public String[] setCSVHeader() {
		return new String[]{"Spinner Value"/*, "Position"*/};
	}
	
	@Override
	public String[] toCSVData() {
		return new String[]{intakeSpinner.get() + ""/*, m_intakeExtender.get() + ""*/};
	}
	
}
