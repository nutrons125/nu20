package frc.robot.subsystems.vision;

import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.Shooter;
import frc.robot.utils.Limelight;
import frc.robot.utils.PolynomialRegression;

import javax.annotation.Nullable;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import static frc.robot.subsystems.vision.Vision.VisionConstants.*;
import static java.lang.Math.*;

/**
 * Class for interfacing with {@link Limelight}s
 */
public class Vision {
	public enum DistanceRegion {
		Close(CLOSE_DISTANCE_REGRESSION, 0), Far(FAR_DISTANCE_REGRESSION, 1);
		private final PolynomialRegression regression;
		private final int pipeline;
		
		PolynomialRegression getRegression() {
			return this.regression;
		}
		
		int getPipeline() {
			return pipeline;
		}
		
		DistanceRegion(PolynomialRegression regression, int pipeline) {
			this.regression = regression;
			this.pipeline = pipeline;
		}
	}
	
	private static Vision instance;
	private Limelight limelight1;
	@Nullable
	private Limelight limelight2; //Variable names still have to be refactored with an appropriate different name
	static ArrayList<Point> pointsLeftOut = new ArrayList<>();
	
	private DistanceRegion currentDistanceRegion = DistanceRegion.Close;
	
	private Vision(int limelightCount) {
		limelight1 = new Limelight(VisionConstants.LIMELIGHT_1_ADDRESS);
		limelight2 = limelightCount == 2 ? new Limelight(VisionConstants.LIMELIGHT_2_ADDRESS) : null;
	}
	
	/**
	 * Gets the instance of {@link Vision}
	 *
	 * @return Vision instance
	 */
	public static Vision getInstance(int... limelightCount) {
		return instance = instance == null ? new Vision(limelightCount[0]) : instance;
	}
	
	/**
	 * Returns the first {@link Limelight} (planning on renaming to a more meaningful name)
	 *
	 * @return First {@link Limelight}
	 */
	public Limelight getLimelight1() {
		return limelight1;
	}
	
	/**
	 * Returns the second {@link Limelight} (planning on renaming to a more meaningful name)
	 *
	 * @return Second {@link Limelight}
	 */
	@SuppressWarnings("unused")
	public Limelight getLimelight2() {
		return limelight2 == null ? limelight1 : limelight2;
	}
	
	/**
	 * Updates all {@link Limelight}s' values
	 */
	public void update() {
		limelight1.update();
		if(limelight2 != null)
			limelight2.update();
		updateShuffleboard();
	}
	
	public void updateXY() {
		limelight1.updateXY();
		if(limelight2 != null)
			limelight2.updateXY();
		updateShuffleboard();
	}
	
	public void setDistanceRegion(DistanceRegion distanceRegion) {
		this.currentDistanceRegion = distanceRegion;
		getLimelight1().setPipeline(distanceRegion.getPipeline());
	}
	
	public void testDistai() {
		// for each point you left out
		for(Point point : pointsLeftOut) {
			double angle = point.x; // the angle
			double expectedDist = point.y; //The distance in feet
			double distanceFromRegression = currentDistanceRegion.getRegression().predict(angle);
			double error = Math.abs(distanceFromRegression - expectedDist);
			System.out.println(error);
		}
	}
	
	public double getDistance() {
		return currentDistanceRegion.getRegression().predict(getLimelight1().getYOffset());
	}
	
	/**
	 * To get pose relative to inner port
	 *
	 * @return Pose of target relative to camera
	 */
	public Pose2d getRobot2TargetPoseInRobotFrame() {
		double hypotenuse = getDistance(),
				theta = Shooter.getTurret().getAbsoluteAngle() + getLimelight1().getXOffset(),
				adjacent = hypotenuse * cos(Math.toRadians(theta)),
				opposite = adjacent * tan(Math.toRadians(theta)),
				newTheta = -Math.toDegrees(atan2(opposite, adjacent + (INNER_PORT_DEPTH / 12))); //If newTheta is too big then target outer goal
		return new Pose2d(new Translation2d(opposite, adjacent + (INNER_PORT_DEPTH / 12)), Rotation2d.fromDegrees(newTheta));
	}
	
	/**
	 * Puts debug info on Shuffleboard
	 */
	public void updateShuffleboard() {
		SmartDashboard.putString("Vision Distance", getDistance() + " feet");
	}
	
	public static class VisionConstants {
		public static final String
				LIMELIGHT_1_ADDRESS = "limelight", //10.1.25.11
				LIMELIGHT_2_ADDRESS = "limelight-left"; //10.1.25.88
		
		public static final double
				INNER_PORT_DEPTH = 0,//29.25/4, // 29.25
				INNER_PORT_OFFSET_LIMIT = 12; //17.549

		public static PolynomialRegression FAR_DISTANCE_REGRESSION, CLOSE_DISTANCE_REGRESSION;
		
		static {
			try(var scan = new Scanner(new File(Filesystem.getDeployDirectory().getPath() + "/data/far-distance-regression-samples.csv"))) {
				Queue<Double> angles = new LinkedList<>(), distances = new LinkedList<>();
				
				while(scan.hasNext()) {
					String[] angleAndDistance = scan.nextLine().split(",");
					angles.add(Double.parseDouble(angleAndDistance[1]));
					distances.add(Double.parseDouble(angleAndDistance[0]));
				}
				
				double[] angleArr = new double[angles.size()], distArr = new double[angleArr.length];
				
				for(int i = 0; i < angleArr.length && angles.size() > 0 && distances.size() > 0; i++) {
					angleArr[i] = angles.poll();
					distArr[i] = distances.poll();
				}
				FAR_DISTANCE_REGRESSION = new PolynomialRegression(angleArr, distArr, 3);
			} catch(FileNotFoundException e) {
				e.printStackTrace();
			}
			
			try(var scan = new Scanner(new File(Filesystem.getDeployDirectory().getPath() + "/data/close-distance-regression-samples.csv"))) {
				Queue<Double> angles = new LinkedList<>(), distances = new LinkedList<>();
				
				while(scan.hasNext()) {
					String[] angleAndDistance = scan.nextLine().split(",");
					double footAndInches = Double.parseDouble(angleAndDistance[0]);
					double foots = Math.floor(footAndInches);
					double inches = ((footAndInches - foots) * 100) + 3.583;//Multiply the feet by 12 add the whole thing to inches
					double feet = (foots * 12 + inches) / 12;
					double a = Double.parseDouble(angleAndDistance[1]);
					
					// Arctan(h/x) = Angle?
					
					if(angleAndDistance.length != 3) {
						angles.add(a);
						distances.add(feet);
					} else {
//						pointsLeftOut.add(new Point(a, feet));
					}
				}
				
				double[] angleArr = new double[angles.size()], distArr = new double[angleArr.length];
				
				for(int i = 0; i < angleArr.length && angles.size() > 0 && distances.size() > 0; i++) {
					angleArr[i] = angles.poll();
					distArr[i] = distances.poll();
				}
				CLOSE_DISTANCE_REGRESSION = new PolynomialRegression(angleArr, distArr, 3);
			} catch(FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}
	}
}
/*6.06, 20.47
7.06, 16.58
8.06, 13.6
9.06, 11.12
10.03, 9.55
10.06, 8.93
11.06, 7.12
12.06, 5.34
13.06, 3.449
14.06, 2.238
15.06, 1.13
16.06, 0.27
17.06, -0.52
18.06, -1.2
19.06, -1.43
20.06, -2.099
21.06, -2.65*/