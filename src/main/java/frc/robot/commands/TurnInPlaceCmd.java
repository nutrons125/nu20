package frc.robot.commands;

import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TurnInPlaceCmd extends CommandBase {
	double angleToTurnDeg;
	double startAngle;
	private static final double P = 0.005, D = 0;
	ProfiledPIDController profiledController;

	public TurnInPlaceCmd(Drivetrain drivetrain, double angleToTurnDeg) {
		addRequirements(drivetrain);
		this.angleToTurnDeg = angleToTurnDeg;
		profiledController = new ProfiledPIDController(P, 0, D, new TrapezoidProfile.Constraints(45, 10));
		profiledController.enableContinuousInput(-180, 180);
		profiledController.setTolerance(2.5);
	}

	public void initialize() {
		startAngle = Drivetrain.getInstance().getAngle();
	}

	@Override
	public void execute() {
		double output = profiledController.calculate(Drivetrain.getInstance().getAngle(), angleToTurnDeg);
		Drivetrain.getInstance().driveNormal(0, -output);
	}

	@Override
	public boolean isFinished() {
		return profiledController.atGoal();
	}

	public void end(boolean interrupted) {
		Drivetrain.getInstance().driveNormal(0, 0);
	}
}
