/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Intake;

public class DrivePath extends CommandBase {
  /**
   * Creates a new drivePath.
   */
  Drivetrain drivetrain;
  Trajectory trajectory; 
  public DrivePath(Drivetrain drivetrain, Trajectory trajectory){
    this.drivetrain = drivetrain;
    this.trajectory = trajectory; 
    addRequirements(drivetrain);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    drivetrain.startFollowingPath(trajectory);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    
    drivetrain.stopDriving();
    
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return drivetrain.doneFollowing();
  }
}
