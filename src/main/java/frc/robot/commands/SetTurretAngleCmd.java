package frc.robot.commands;/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/


import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;
public class SetTurretAngleCmd extends CommandBase {
	private final double angle;
	
	public SetTurretAngleCmd(double angle) {
		this.angle = angle;
	}
	
	@Override
	public void initialize() {
		Shooter.getTurret().setAngle(angle);
	}
	
	@Override
	public boolean isFinished() {
		return true;
	}
}






















