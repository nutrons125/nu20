package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class SmartAimCmd extends CommandBase {

    public SmartAimCmd() {
        super();
    }

    @Override
    public void initialize() {

    }

    @Override
    public void execute() {
        Shooter.getTurret().smartSet();
    }

    @Override
    public void end(boolean interrupted) {

    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
