/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Shooter;
public class ShootCmd extends CommandBase {
	Shooter shooter;
	
	public ShootCmd() {
    shooter = Shooter.getInstance();
    addRequirements(shooter);
	}
	
	@Override
	public void initialize() {
    Intake.getInstance().run(1);
//    Shooter.getHood().controlHood(0.82);
		shooter.setState(Shooter.Goal.LOCKED_ON);
	}
	
	@Override
	public void execute() {
		Shooter.getHood().magSet(295);
		if (shooter.getStatus().equals(Shooter.LOCKED_ON)) {
			shooter.setState(Shooter.Goal.SHOOT);
		}
	}
	
	@Override
	public boolean isFinished() {
		return false;
	}
	
	@Override
	public void end(boolean interrupted) {
		shooter.setState(Shooter.Goal.IDLE);
	}
}