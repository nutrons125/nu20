/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;

public class IntakeCmd extends CommandBase {
    final boolean out;

    public IntakeCmd(boolean out) {
        this.out = out;
        addRequirements(Intake.getInstance());
    }

    @Override
    public void initialize() {
        if (out) {
            Intake.getInstance().setExtended(true);
            Intake.getInstance().run(1);
        } else {
            Intake.getInstance().setExtended(false);
            Intake.getInstance().run(0);
        }
    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public void end(boolean interrupted) {
    }
}