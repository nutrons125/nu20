package frc.robot.testrobots;

import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.OI;
import frc.robot.subsystems.Drivetrain;
import frc.robot.utils.Limelight;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class DistanceSampleRobot extends TimedRobot {
	Drivetrain drivetrain;
	Limelight limelight;
	Controller driver;
	PrintWriter sampleWriter;
	File sampleFile;
	
	@Override
	public void robotInit() {
		limelight = Vision.getInstance(1).getLimelight1();
		drivetrain = Drivetrain.getInstance();
		driver = OI.getInstance().getDriverController();
		File root = Filesystem.getOperatingDirectory();
		File sampleDirectory = new File(root.getPath() + "/samples");
		
		if(!sampleDirectory.exists()) {
			sampleDirectory.mkdir();
		}
		
		this.sampleFile = new File(sampleDirectory.getPath() + "/far-distance-regression-samples.csv");
		try {
			if(!sampleFile.exists()) {
				sampleFile.createNewFile();
			}
			sampleWriter = new PrintWriter(sampleFile);
		} catch(IOException e) {
			e.printStackTrace();
		}
		SmartDashboard.putString("File Contents", getFileContents());
	}
	
	
	@Override
	public void robotPeriodic() {
		Vision.getInstance().updateXY();
		
		if(driver.getStart()) {
			drivetrain.resetOdometry();
		}
		
		if(driver.getRightStickPress()) {
			deleteAllData();
		}
		
		if(driver.a()) {
			writeSample();
		}
	}
	
	@Override
	public void teleopInit() {
		SmartDashboard.putString("File Contents", getFileContents());
	}
	
	@Override
	public void teleopPeriodic() {
		drivetrain.periodic();
	}
	
	@Override
	public void disabledInit() {
		SmartDashboard.putString("File Contents", getFileContents());
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	void writeSample() {
		String sample = limelight.getYOffset() + "," + Drivetrain.getInstance().getCurrentPose().getTranslation().getY();
		System.out.println("SAMPLE:\n" + sample + "\n");
		SmartDashboard.putString("Sample", sample);
		sampleWriter.append("\n" + sample);
	}
	
	void deleteAllData() {
		sampleWriter.close();
		try(var deleter = new PrintWriter(sampleFile)) {
			deleter.write("");
			deleter.close();
			sampleWriter = new PrintWriter(sampleFile);
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
	}
	
	String getFileContents() {
		try(var scan = new Scanner(sampleFile)) {
			var contents = new StringBuilder();
			while(scan.hasNext()) {
				contents.append(scan.nextLine()).append("\n");
			}
			return contents.toString();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		return "Couldn't get file contents";
	}
	
	public void updateShuffleboard() {
		SmartDashboard.putNumber("Y Offset", Vision.getInstance().getLimelight1().getYOffset());
		SmartDashboard.putNumber("Odometry Distance", Drivetrain.getInstance().getCurrentPose().getTranslation().getY());
		SmartDashboard.putNumber("Heading", Drivetrain.getInstance().getCurrentPose().getRotation().getDegrees());
	}
}
