package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.utils.Limelight;
import frc.robot.subsystems.vision.Vision;

public class VisionTestRobot extends TimedRobot {
	Vision vision;
	Limelight limelight;

	@Override
	public void robotInit() {
		SmartDashboard.putBoolean("Far Region", false);
		vision = Vision.getInstance(1);
		limelight = Vision.getInstance().getLimelight1();
	}
	
	@Override
	public void robotPeriodic() {
		vision.update();
		if (SmartDashboard.getBoolean("Far Region", false)) {
			vision.setDistanceRegion(Vision.DistanceRegion.Far);
		} else {
			vision.setDistanceRegion(Vision.DistanceRegion.Close);
		}
		
	}
	
	@Override
	public void autonomousInit() {
	}
	
	@Override
	public void autonomousPeriodic() {
	}
	
	@Override
	public void teleopInit() {
	}
	
	@Override
	public void teleopPeriodic() {
	}
	
	@Override
	public void disabledInit() {
	}
	
	public void disabledPeriodic() {
	}
	
	public void updateShuffleboard() {
	}
	
}
