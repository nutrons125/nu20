package frc.robot.testrobots;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import frc.robot.logging.CSVWriter;
import frc.robot.logging.Match;
import frc.robot.subsystems.Drivetrain;

import java.util.Arrays;

public class LogTestRobot extends TimedRobot {
	CSVWriter[] writers;
	Drivetrain drivetrain;

	@Override
	public void robotInit() {
	/*	writers = new CSVWriter[]{new CSVWriter(Shooter.getInstance(), Match.NO_MATCH),
				new CSVWriter(Intake.getInstance(), Match.NO_MATCH),
				new CSVWriter(Feeder.getInstance(), Match.NO_MATCH),
				new CSVWriter(Drivetrain.getInstance(), Match.NO_MATCH),
				new CSVWriter(Climber.getInstance(), Match.NO_MATCH)};*/
		drivetrain = Drivetrain.getInstance();
		writers = new CSVWriter[]{new CSVWriter(drivetrain, Match.NO_MATCH)};
//		writeLogs();
	}
	
	@Override
	public void robotPeriodic() {
	}
	
	@Override
	public void autonomousInit() {
	}
	
	@Override
	public void autonomousPeriodic() {
	}
	
	@Override
	public void teleopInit() {
		startLogs();
	}
	
	@Override
	public void teleopPeriodic() {
		drivetrain.periodic();
		updateLogging();
	}
	
	@Override
	public void disabledInit() {
	
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	public void updateShuffleboard() {
	}
	
	public void updateLogging() {
		Arrays.asList(writers).forEach(writer -> writer.addData(DriverStation.getInstance().getMatchTime()));
	}
	
	public void startLogs() {
		Arrays.asList(writers).forEach(CSVWriter::startLogging);
	}
}
