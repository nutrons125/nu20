package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.*;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.Controller;
import frc.robot.utils.SmartNumber;

public class HoodTestRobot extends TimedRobot {
	Shooter shooter;
	Shooter.Hood hood;
	Shooter.Turret turret;
	Accelerator accelerator;
	Feeder feeder;
	SmartNumber setpoint;
	Controller driverController;
	Superstructure superstructure;
	Vision vision;
	Intake intake;
	Drivetrain drivetrain;

	@Override
	public void robotInit() {
		superstructure = Superstructure.getInstance();
		vision = Vision.getInstance(1);
		driverController = new Controller(0);
		hood = Shooter.getHood();
		shooter = Shooter.getInstance();
		turret = Shooter.getTurret();
		accelerator = Accelerator.getInstance();
		feeder = Feeder.getInstance();
		intake = Intake.getInstance();
		drivetrain = Drivetrain.getInstance();
		setpoint = new SmartNumber("Servo Setpoint", 330);
		superstructure.ledsOn();
		shooter.overrideHood = true;
		superstructure.setRPMThroughDash = false;
	}

	@Override
	public void robotPeriodic() {
		vision.updateXY();
		SmartDashboard.putNumber("Accelerator position", Accelerator.getInstance().getEncoderPosition());
		SmartDashboard.putNumber("Hood Angle", hood.getPosition());
		SmartDashboard.putNumber("Shooter RPM", shooter.getShooterRPM());
		SmartDashboard.putNumber("Shooter reference", shooter.shooterReference);
		SmartDashboard.putNumber("Shoot setpoint", shooter.shootSetpoint);
		SmartDashboard.putBoolean("Target Found", vision.getLimelight1().targetFound());
		SmartDashboard.putString("Shooter State", shooter.currentGoal.name());

		if (driverController.getBack()) {
			shooter.shootSetpoint = Shooter.FAR_RPM;
			Vision.getInstance().setDistanceRegion(Vision.DistanceRegion.Far);
		} else if (driverController.getStart()) {
			shooter.shootSetpoint = Shooter.CLOSE_RPM;
			Vision.getInstance().setDistanceRegion(Vision.DistanceRegion.Close);
		}
	}

	@Override
	public void teleopInit() {
	}

	@Override
	public void teleopPeriodic() {
		hood.magSet(setpoint.getValue());


		if (driverController.getRightBumper()) {
/*
			if (!Vision.getInstance().getLimelight1().targetFound()) {
				shooter.setShooterVel(4000);
				feeder.run(Feeder.FEED_SHOOTER_POWER);
				accelerator.run(Accelerator.FEED_SHOOTER_POWER);
				intake.run((int) Intake.INTAKE_SPEED);
			} else {
*/
				shooter.setState(Shooter.Goal.SHOOT);
/*
			}
*/
		} else if (driverController.getLeftBumper()) {
			shooter.setState(Shooter.Goal.LOCKED_ON);
		} else {
			shooter.setState(Shooter.Goal.IDLE);
			turret.incrementAngle(-driverController.getRightStickX() * 20);
			feeder.run(0);
			accelerator.run(0);
			intake.run(0);
		}

		shooter.updateStateMachine();

		drivetrain.periodic();
	}

	@Override
	public void disabledInit() {
	}

	@Override
	public void disabledPeriodic() {
		setpoint.setValue(setpoint.getValue());
	}
}
