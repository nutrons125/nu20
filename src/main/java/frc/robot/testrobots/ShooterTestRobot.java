package frc.robot.testrobots;

import com.revrobotics.ControlType;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Superstructure;

public class ShooterTestRobot extends TimedRobot {
	private Shooter shooter;
	private Superstructure superstructure;
	private ControlType controlType = ControlType.kSmartVelocity;
	
	
	@Override
	public void robotInit() {
		superstructure = Superstructure.getInstance();
		shooter = Shooter.getInstance();
		shooter.enableCoastMode();
		SmartDashboard.putNumber("100 RPM roundtrip", shooter.tp100ms2rpm(shooter.rpm2ticksPer100ms(100)));
		SmartDashboard.putNumber("Set P", shooter.localP);
		SmartDashboard.putNumber("Set I", shooter.localI);
		SmartDashboard.putNumber("Set D", shooter.localD);
		SmartDashboard.putNumber("Set FF", shooter.localFF);
		SmartDashboard.putNumber("Set I Zone", shooter.localIZone);
		
//		SmartDashboard.putNumber("Set feeder P", shooter.bLocalP);
//		SmartDashboard.putNumber("Set feeder I", shooter.bLocalI);
//		SmartDashboard.putNumber("Set feeder D", shooter.bLocalD);
//		SmartDashboard.putNumber("Set feeder FF", shooter.bLocalFF);
//		SmartDashboard.putNumber("Set feeder I Zone", shooter.bLocalIZone);
		
		SmartDashboard.putNumber("Set shooter goal RPM", 0);
		SmartDashboard.putNumber("Set shooter duty cycle", 0);
		
		SmartDashboard.putNumber("Set feeder goal RPM", 0);
		SmartDashboard.putNumber("Set feeder duty cycle", 0);
		
		SmartDashboard.putBoolean("Use velocity references", true);
		
		SmartDashboard.putNumber("Run Option", getOptionFromMode(controlType));
	}
	
	@Override
	public void robotPeriodic() {
/*
		shooter.setShooterP(SmartDashboard.getNumber("Set P", shooter.localP));
		shooter.setShooterI(SmartDashboard.getNumber("Set I", shooter.localI));
		shooter.setShooterD(SmartDashboard.getNumber("Set D", shooter.localD));
		shooter.setShooterFF(SmartDashboard.getNumber("Set FF", shooter.localFF));
		shooter.setShooterIZone(SmartDashboard.getNumber("Set I Zone", shooter.localIZone));
		
		shooter.setBoosterP(SmartDashboard.getNumber("Set feeder P", shooter.bLocalP));
		shooter.setBoosterI(SmartDashboard.getNumber("Set feeder I", shooter.bLocalI));
		shooter.setBoosterD(SmartDashboard.getNumber("Set feeder D", shooter.bLocalD));
		shooter.setBoosterFF(SmartDashboard.getNumber("Set feeder FF", shooter.bLocalFF));
		shooter.setBoosterIZone(SmartDashboard.getNumber("Set feeder I Zone", shooter.bLocalIZone));
		
		SmartDashboard.putNumber("Shooter Velocity Error", shooter.getShooterClosedLoopError());
		SmartDashboard.putNumber("Shooter RPM", shooter.getShooterRPM());
		SmartDashboard.putNumber("Shooter Applied Output", shooter.getMainShooterAppliedOutput());
		SmartDashboard.putNumber("Shooter Output Current", shooter.getMainShooterOutputCurrent());
		
		SmartDashboard.putNumber("Feeder Output Current", shooter.getBoosterOutputCurrent());
		SmartDashboard.putNumber("Feeder RPM", shooter.getBoosterRPM());
		SmartDashboard.putNumber("Feeder Velocity Error", shooter.getBoosterClosedLoopError());
		
		int runOption = (int) SmartDashboard.getNumber("Run Option", getOptionFromMode(controlType));
		controlType = getModeFromOption(runOption);
		SmartDashboard.putNumber("Run Option", getOptionFromMode(controlType));
*/
	}
	
	@Override
	public void teleopInit() {
	}
	
	@Override
	public void teleopPeriodic() {
/*
		if(controlType == ControlType.kDutyCycle) {
			shooter.setShooterDutyCycle(SmartDashboard.getNumber("Set shooter duty cycle", shooter.shooterReference));
			shooter.setBoosterDutyCycle(SmartDashboard.getNumber("Set feeder duty cycle", shooter.boosterReference));
		} else {
			shooter.setShooterVel(SmartDashboard.getNumber("Set shooter goal RPM", shooter.shooterReference));
			shooter.setBoosterVel(SmartDashboard.getNumber("Set feeder goal RPM", shooter.boosterReference));
		}
*/
	}
	
	private static ControlType getModeFromOption(int option) {
		switch(option) {
			case 2:
				return ControlType.kSmartVelocity;
			case 3:
				return ControlType.kDutyCycle;
			default:
				return ControlType.kVelocity;
		}
	}
	
	private static int getOptionFromMode(ControlType controlType) {
		switch(controlType) {
			case kSmartVelocity:
				return 2;
			case kDutyCycle:
				return 3;
			default:
				return 1;
		}
	}
}
