package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;

public class DJRobot extends TimedRobot {
	protected DJRobot() {
	}
	
	@Override
	public void robotInit() {
	
	}
	
	@Override
	public void robotPeriodic() {
	}
	
	@Override
	public void teleopInit() {
	}
	
	@Override
	public void teleopPeriodic() {
	}
	
	@Override
	public void disabledInit() {
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	@Override
	public void autonomousPeriodic() {
	}
}
