package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.OI;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.MovingAverage;
import frc.robot.utils.SmartNumber;

public class TurretTestRobot extends TimedRobot {
	Shooter.Turret turret;
	SmartNumber dashTurretCommand;
	SmartNumber dashMA;
	Drivetrain drivetrain;
	
	@Override
	public void robotInit() {
		drivetrain = Drivetrain.getInstance();
		turret = Shooter.getTurret();
		dashTurretCommand = new SmartNumber("Turret command", 0);
		dashMA = new SmartNumber("Direction sign moving average", 0);
		OI.getInstance().getOperatorController().setRightStickXDeadband(0.05);
	}
	
	@Override
	public void disabledInit() {
	}
	
	@Override
	public void autonomousInit() {
	}
	
	@Override
	public void robotPeriodic() {
		Vision.getInstance(1).update();
		SmartDashboard.putNumber("Turret Angle", turret.getAngle());
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	@Override
	public void autonomousPeriodic() {
		super.autonomousPeriodic();
	}
	
	@Override
	public void teleopInit() {
		turret.resetEncoder();
	}
	boolean buttonHeld = false;
	@Override
	public void teleopPeriodic() {
		drivetrain.periodic();
		
		if (OI.getInstance().getDriverController().b()) {
			turret.aimInner();
		}

//		turret.setPower(turnValue);
//		dashTurretCommand.setValue(turnValue);
//		if (OI.getInstance().getOperatorController().a()) {
//			turret.incrementAngle(10);
//			turret.setAngle(60);
//		}
		
//		turret.incrementAngle(turnValue * 5);
//		dashTurretCommand.setValue(turnValue * 5);

		if (OI.getInstance().getDriverController().a()) {
			if (!buttonHeld) {
				turret.incrementAngle(5);
				buttonHeld = true;
			}
		} else {
			buttonHeld = false;
		}
		
/*
		if (OI.getInstance().getOperatorController().b()) {
			turret.alignTurret();
		}
*/

//		stickDirectionTurn();
	}
	
	MovingAverage signMovingAverage = new MovingAverage(0.1); //To account for unread stick input
	
	{
		signMovingAverage.update(0);
	}
	
	/**
	 * Sets the turret to the left sticks direction bounded by  +-270
	 */
	public void stickDirectionTurn() {
		dashMA.setValue(signMovingAverage.get());
		double stickAngle = OI.getInstance().getOperatorController().getRightStickDirection(0.7);
		SmartDashboard.putNumber("Right Stick Angle", stickAngle);
		boolean bottomLeft = stickAngle <= -120 && stickAngle >= -179.9999;
		boolean bottomRight = stickAngle >= 120 && stickAngle <= 180;
		
		double turretCommand = 0;
		//Lmoa
		getAngleCommand:
		{
			if(Math.signum(signMovingAverage.get()) == 1 && bottomLeft) { // If the stick went from the bottom right to the bottom left then let the angle go to -270
				turretCommand = stickAngle + 360;
				break getAngleCommand;
			} else if(Math.signum(signMovingAverage.get()) == -1 && bottomRight) { // If the stick went from the bottom left to the bottom right then let the angle go to 270
				turretCommand = stickAngle - 360;
				break getAngleCommand;
			}
			
			turretCommand = stickAngle == 0 ? turret.getAngle() : stickAngle;
			signMovingAverage.update(Math.signum(stickAngle));
		}
		turret.setAngle(turretCommand);
		dashTurretCommand.setValue(turretCommand);
	}
}
