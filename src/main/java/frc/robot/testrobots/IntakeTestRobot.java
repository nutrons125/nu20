/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;
import frc.robot.OI;
import frc.robot.subsystems.Intake;

public class IntakeTestRobot extends TimedRobot {
	private Intake intake;
	@Override
	public void robotInit() {
		intake = Intake.getInstance();
	}
	
	@Override
	public void robotPeriodic() {
	}
	
	@Override
	public void autonomousInit() {
	}
	
	@Override
	public void autonomousPeriodic() {
	}
	
	@Override
	public void teleopInit() {
	}
	
	@Override
	public void teleopPeriodic() {
		intake.run(OI.getInstance().getOperatorController().a() ? 1 : 0);
	}

	@Override
	public void disabledInit() {
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	public void updateShuffleboard() {
	}
}
