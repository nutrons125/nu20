/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.testrobots;

import edu.wpi.first.wpilibj.TimedRobot;
import frc.robot.subsystems.Feeder;

public class FeederTestRobot extends TimedRobot {
	private Feeder feeder;

	@Override
	public void robotInit() {
//		feeder = Feeder.getInstance();
	}
	
	@Override
	public void robotPeriodic() {
		feeder.periodic();
	}
	
	@Override
	public void autonomousInit() {
	}
	
	@Override
	public void autonomousPeriodic() {
	}
	
	@Override
	public void teleopInit() {
//		feeder.runTeleOp();
	}
	
	@Override
	public void teleopPeriodic() {
	}
	
	@Override
	public void disabledInit() {
	}
	
	@Override
	public void disabledPeriodic() {
	}
	
	public void updateShuffleboard() {
	}
}
