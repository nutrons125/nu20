package frc.robot.logging;

import edu.wpi.first.wpilibj.DriverStation;

public class Match {
	public static final Match NO_MATCH = new Match("Meeting", DriverStation.MatchType.None, 0);
	private final DriverStation.MatchType matchType;
	private final String eventName;
	private final int matchNumber;
	
	public Match(String eventName, DriverStation.MatchType matchType, int matchNumber) {
		this.eventName = eventName;
		this.matchNumber = matchNumber;
		this.matchType = matchType;
	}
	
	public static Match getCurrentMatch() {
		var ds = DriverStation.getInstance();
		return new Match(ds.getEventName(), ds.getMatchType(), ds.getMatchNumber());
	}
	
	String getEventName() {
		return eventName;
	}
	
	int getMatchNumber() {
		return matchNumber;
	}
	
	DriverStation.MatchType getMatchType() {
		return this.matchType;
	}
}
