package frc.robot.logging;

import edu.wpi.first.wpilibj.DriverStation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * This class logs information about classes to match specific files
 */
public class CSVWriter {
	//	private static final String LOG_ROOT = "/home/lvuser/logs";
	private static final String LOG_ROOT = "/u";
	private volatile ConcurrentLinkedDeque<String> linesToWrite = new ConcurrentLinkedDeque<>();
	private Loggable loggable;
	private PrintWriter printWriter;
	private boolean logging = false;
	
	/**
	 * Constructs a new CSVWriter from a {@link Loggable} class and a {@link Match}
	 *
	 * @param classToAnalyze {@link Loggable} object class to write information from
	 * @param match          Provides match information and a path to write the classToAnalyze's log file
	 */
	public CSVWriter(Loggable classToAnalyze, Match match) {
		this.loggable = Objects.requireNonNull(classToAnalyze);
		var directory = String.format("%s/%s/%s-%d/",
				LOG_ROOT, match.getEventName().replace(' ', '-'), match.getMatchType().name(), match.getMatchNumber());
		var logFile = new File(directory);
		System.out.println("logFile.mkdirs() = " + logFile.mkdirs());
		var subFile = new File(directory + classToAnalyze.getClass().getSimpleName() + ".csv");
		try {
			this.printWriter = new PrintWriter(subFile);
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
		if(!subFile.exists()) {
			var headBuilder = new StringBuilder("Time, ");
			Arrays.asList(classToAnalyze.setCSVHeader()).forEach(cell -> headBuilder.append(cell).append(", "));
			writeLine(headBuilder.toString());
		}
	}
	
	/**
	 * Queues another line of variable values for writing to match's log file
	 *
	 * @param matchTime Match time from the {@link DriverStation}
	 */
	public synchronized void addData(double matchTime) {
		var lineBuilder = new StringBuilder(matchTime + ", ");
		Arrays.asList(loggable.toCSVData()).forEach(cell -> lineBuilder.append(cell).append(", "));
		linesToWrite.add(lineBuilder.toString());
	}
	
	/**
	 * Simply prints a string to the log file
	 *
	 * @param line String to write
	 */
	public void writeLine(String line) {
		printWriter.println(line);
		System.out.println("line = " + line);
		flush();
	}
	
	/**
	 * Starts another process that writes all queued data to the match's log file
	 */
	public void startLogging() {
		logging = true;
		new Thread(() -> {
			while(!DriverStation.getInstance().isDisabled() && logging) {
				while(linesToWrite.size() > 0 && logging) {
					var line = linesToWrite.pollFirst();
					if(line == null)
						break;
					writeLine(line);
				}
				try {
					Thread.sleep(100);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	/**
	 * Flushes all data to the match's log file
	 */
	public void flush() {
		startLogging();
		printWriter.flush();
	}
	
	public void stopLogging() {
		logging = false;
	}
	
	public void stopLoggingAndClose() {
		logging = false;
		printWriter.close();
	}
}
