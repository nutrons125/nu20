package frc.robot.logging;

import jdk.jshell.spi.ExecutionControl;

import java.util.Arrays;

public interface Loggable {
	String[] setCSVHeader();
	
	String[] toCSVData();
	
	default String[] getCSVHeader()  {
		String[] header = setCSVHeader();
		return header;
	}
	
	default String[] getCSVData()  {
		String[] data = toCSVData();
		return data;
	}
}
