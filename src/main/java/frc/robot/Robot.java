/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Superstructure;
import frc.robot.utils.Limelight;
import frc.robot.subsystems.vision.Vision;
import frc.robot.utils.SmartBoolean;
import frc.robot.utils.SmartNumber;

public class Robot extends TimedRobot {
    Superstructure superstructure;
    SmartNumber sdFeedforward;
    Shooter.Hood hood;
    SmartNumber setpoint;
    SmartBoolean smartSetButton;
    SmartNumber climberTicks;

    @Override
    public void robotInit() {
        superstructure = Superstructure.getInstance();
        OI.getInstance().setXBoxDeadbands(false);
        sdFeedforward = new SmartNumber("Set FF", Shooter.ShooterConstants.FF);
        hood = Shooter.getHood();
        setpoint = new SmartNumber("Servo Setpoint", 1);
        smartSetButton = new SmartBoolean("Reset Smart Set Pose", false);
        climberTicks = new SmartNumber("Climber ticks", -1);
        superstructure.climberMotor.setNeutralMode(NeutralMode.Brake);
    }

    @Override
    public void robotPeriodic() {
        Vision.getInstance(1).updateXY();
//		Superstructure.getInstance().ledsOn();
        updateShuffleboard();
        Superstructure.getInstance().periodic();

        if(smartSetButton.getValue()) {
            superstructure.resetSmartSetPosition();
            smartSetButton.setValue(false);
        }
        climberTicks.setValue(superstructure.climberMotor.getSelectedSensorPosition());
    }

    @Override
    public void autonomousInit() {
        superstructure.initAuto();
    }

    @Override
    public void autonomousPeriodic() {
        CommandScheduler.getInstance().run();
    }

    @Override
    public void teleopInit() {
        superstructure.climberMotor.setNeutralMode(NeutralMode.Brake);

        SmartDashboard.putNumber("Set FF", localFF);
        Drivetrain.getInstance().enterTeleop();
    }

    @Override
    public void teleopPeriodic() {
        superstructure.runTeleOp();
//		hood.magSet(setpoint.getValue());
    }

    @Override
    public void disabledInit() {
        Vision.getInstance(1).getLimelight1().setLEDMode(Limelight.LEDMode.Off);
        Shooter.getInstance().setState(Shooter.Goal.IDLE);
        Drivetrain.getInstance().setBrake();
        superstructure.climberMotor.setNeutralMode(NeutralMode.Brake);
    }

    double localFF = Shooter.ShooterConstants.FF;
    double shooterSetpoint = 4200;

    // ToDo: Make intake stay on while retracting it
    @Override
    public void testInit() {

    }

    @Override
    public void testPeriodic() {
        SmartDashboard.putNumber("Shooter RPM", Shooter.getInstance().getShooterRPM());
        if (Shooter.getInstance().getShooterRPM() < 1000) {
			Shooter.getInstance().setShooterVel(shooterSetpoint);
		} else {
        	Shooter.getInstance().setShooterVelNoRamp(shooterSetpoint);
		}

        if (OI.getInstance().getDriverController().yPressed()) {
            shooterSetpoint += 150;
        } else if (OI.getInstance().getDriverController().aPressed()) {
            shooterSetpoint -= 150;
        }
        SmartDashboard.putNumber("Shooter Setpoint", shooterSetpoint);
    }


    @Override
    public void disabledPeriodic() {
        Drivetrain.getInstance().periodic();
        setpoint.setValue(setpoint.getValue());
        Superstructure.getInstance().ledsOn();

    }

    public void updateShuffleboard() {
        SmartDashboard.putNumber("Turret Angle", Shooter.getTurret().getAngle());
    }
}
